# Generated from WedflowLexer.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\27")
        buf.write("\u00d4\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3")
        buf.write("\2\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\3")
        buf.write("\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\b\3\b")
        buf.write("\7\b\u0083\n\b\f\b\16\b\u0086\13\b\3\t\6\t\u0089\n\t\r")
        buf.write("\t\16\t\u008a\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\3\16\3")
        buf.write("\16\3\17\3\17\3\20\3\20\3\21\3\21\3\22\3\22\3\22\3\22")
        buf.write("\5\22\u00a1\n\22\3\22\7\22\u00a4\n\22\f\22\16\22\u00a7")
        buf.write("\13\22\3\22\5\22\u00aa\n\22\3\22\3\22\3\22\3\22\3\23\3")
        buf.write("\23\3\23\5\23\u00b3\n\23\3\24\3\24\3\25\3\25\3\26\5\26")
        buf.write("\u00ba\n\26\3\26\3\26\3\27\6\27\u00bf\n\27\r\27\16\27")
        buf.write("\u00c0\3\27\3\27\3\30\3\30\3\30\7\30\u00c8\n\30\f\30\16")
        buf.write("\30\u00cb\13\30\3\30\3\30\6\30\u00cf\n\30\r\30\16\30\u00d0")
        buf.write("\3\30\3\30\4\u00a5\u00d0\2\31\3\3\5\4\7\5\t\6\13\7\r\b")
        buf.write("\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22")
        buf.write("#\23%\2\'\2)\24+\25-\26/\27\3\2\27\4\2YYyy\4\2GGgg\4\2")
        buf.write("FFff\4\2CCcc\4\2VVvv\4\2TTtt\4\2KKkk\4\2DDdd\4\2WWww\4")
        buf.write("\2UUuu\4\2EEee\4\2QQqq\4\2PPpp\4\2IIii\4\2HHhh\4\2NNn")
        buf.write("n\3\2C\\\4\2C\\c|\4\2//aa\3\2\62;\3\2\13\13\2\u00dd\2")
        buf.write("\3\3\2\2\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3")
        buf.write("\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2")
        buf.write("\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2")
        buf.write("\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2)")
        buf.write("\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\3\61\3\2\2\2")
        buf.write("\5A\3\2\2\2\7Q\3\2\2\2\tb\3\2\2\2\13p\3\2\2\2\rz\3\2\2")
        buf.write("\2\17\u0080\3\2\2\2\21\u0088\3\2\2\2\23\u008c\3\2\2\2")
        buf.write("\25\u008e\3\2\2\2\27\u0090\3\2\2\2\31\u0092\3\2\2\2\33")
        buf.write("\u0094\3\2\2\2\35\u0096\3\2\2\2\37\u0098\3\2\2\2!\u009a")
        buf.write("\3\2\2\2#\u00a0\3\2\2\2%\u00b2\3\2\2\2\'\u00b4\3\2\2\2")
        buf.write(")\u00b6\3\2\2\2+\u00b9\3\2\2\2-\u00be\3\2\2\2/\u00c4\3")
        buf.write("\2\2\2\61\62\7B\2\2\62\63\t\2\2\2\63\64\t\3\2\2\64\65")
        buf.write("\t\4\2\2\65\66\7/\2\2\66\67\t\5\2\2\678\t\6\2\289\t\6")
        buf.write("\2\29:\t\7\2\2:;\t\b\2\2;<\t\t\2\2<=\t\n\2\2=>\t\6\2\2")
        buf.write(">?\t\3\2\2?@\t\13\2\2@\4\3\2\2\2AB\7B\2\2BC\t\2\2\2CD")
        buf.write("\t\3\2\2DE\t\4\2\2EF\7/\2\2FG\t\f\2\2GH\t\r\2\2HI\t\16")
        buf.write("\2\2IJ\t\4\2\2JK\t\b\2\2KL\t\6\2\2LM\t\b\2\2MN\t\r\2\2")
        buf.write("NO\t\16\2\2OP\t\13\2\2P\6\3\2\2\2QR\7B\2\2RS\t\2\2\2S")
        buf.write("T\t\3\2\2TU\t\4\2\2UV\7/\2\2VW\t\6\2\2WX\t\7\2\2XY\t\5")
        buf.write("\2\2YZ\t\16\2\2Z[\t\13\2\2[\\\t\b\2\2\\]\t\6\2\2]^\t\b")
        buf.write("\2\2^_\t\r\2\2_`\t\16\2\2`a\t\13\2\2a\b\3\2\2\2bc\7B\2")
        buf.write("\2cd\t\2\2\2de\t\3\2\2ef\t\4\2\2fg\7/\2\2gh\t\6\2\2hi")
        buf.write("\t\7\2\2ij\t\b\2\2jk\t\17\2\2kl\t\17\2\2lm\t\3\2\2mn\t")
        buf.write("\7\2\2no\t\13\2\2o\n\3\2\2\2pq\7B\2\2qr\t\2\2\2rs\t\3")
        buf.write("\2\2st\t\4\2\2tu\7/\2\2uv\t\20\2\2vw\t\21\2\2wx\t\r\2")
        buf.write("\2xy\t\2\2\2y\f\3\2\2\2z{\7B\2\2{|\t\5\2\2|}\t\2\2\2}")
        buf.write("~\t\b\2\2~\177\t\f\2\2\177\16\3\2\2\2\u0080\u0084\t\22")
        buf.write("\2\2\u0081\u0083\5%\23\2\u0082\u0081\3\2\2\2\u0083\u0086")
        buf.write("\3\2\2\2\u0084\u0082\3\2\2\2\u0084\u0085\3\2\2\2\u0085")
        buf.write("\20\3\2\2\2\u0086\u0084\3\2\2\2\u0087\u0089\5%\23\2\u0088")
        buf.write("\u0087\3\2\2\2\u0089\u008a\3\2\2\2\u008a\u0088\3\2\2\2")
        buf.write("\u008a\u008b\3\2\2\2\u008b\22\3\2\2\2\u008c\u008d\7<\2")
        buf.write("\2\u008d\24\3\2\2\2\u008e\u008f\7?\2\2\u008f\26\3\2\2")
        buf.write("\2\u0090\u0091\7=\2\2\u0091\30\3\2\2\2\u0092\u0093\7.")
        buf.write("\2\2\u0093\32\3\2\2\2\u0094\u0095\7*\2\2\u0095\34\3\2")
        buf.write("\2\2\u0096\u0097\7+\2\2\u0097\36\3\2\2\2\u0098\u0099\7")
        buf.write("}\2\2\u0099 \3\2\2\2\u009a\u009b\7\177\2\2\u009b\"\3\2")
        buf.write("\2\2\u009c\u009d\7\61\2\2\u009d\u00a1\7\61\2\2\u009e\u009f")
        buf.write("\7/\2\2\u009f\u00a1\7/\2\2\u00a0\u009c\3\2\2\2\u00a0\u009e")
        buf.write("\3\2\2\2\u00a1\u00a5\3\2\2\2\u00a2\u00a4\13\2\2\2\u00a3")
        buf.write("\u00a2\3\2\2\2\u00a4\u00a7\3\2\2\2\u00a5\u00a6\3\2\2\2")
        buf.write("\u00a5\u00a3\3\2\2\2\u00a6\u00a9\3\2\2\2\u00a7\u00a5\3")
        buf.write("\2\2\2\u00a8\u00aa\7\17\2\2\u00a9\u00a8\3\2\2\2\u00a9")
        buf.write("\u00aa\3\2\2\2\u00aa\u00ab\3\2\2\2\u00ab\u00ac\7\f\2\2")
        buf.write("\u00ac\u00ad\3\2\2\2\u00ad\u00ae\b\22\2\2\u00ae$\3\2\2")
        buf.write("\2\u00af\u00b3\t\23\2\2\u00b0\u00b3\5\'\24\2\u00b1\u00b3")
        buf.write("\t\24\2\2\u00b2\u00af\3\2\2\2\u00b2\u00b0\3\2\2\2\u00b2")
        buf.write("\u00b1\3\2\2\2\u00b3&\3\2\2\2\u00b4\u00b5\t\25\2\2\u00b5")
        buf.write("(\3\2\2\2\u00b6\u00b7\7\"\2\2\u00b7*\3\2\2\2\u00b8\u00ba")
        buf.write("\7\17\2\2\u00b9\u00b8\3\2\2\2\u00b9\u00ba\3\2\2\2\u00ba")
        buf.write("\u00bb\3\2\2\2\u00bb\u00bc\7\f\2\2\u00bc,\3\2\2\2\u00bd")
        buf.write("\u00bf\t\26\2\2\u00be\u00bd\3\2\2\2\u00bf\u00c0\3\2\2")
        buf.write("\2\u00c0\u00be\3\2\2\2\u00c0\u00c1\3\2\2\2\u00c1\u00c2")
        buf.write("\3\2\2\2\u00c2\u00c3\b\27\2\2\u00c3.\3\2\2\2\u00c4\u00c9")
        buf.write("\5\21\t\2\u00c5\u00c8\5)\25\2\u00c6\u00c8\5+\26\2\u00c7")
        buf.write("\u00c5\3\2\2\2\u00c7\u00c6\3\2\2\2\u00c8\u00cb\3\2\2\2")
        buf.write("\u00c9\u00c7\3\2\2\2\u00c9\u00ca\3\2\2\2\u00ca\u00cc\3")
        buf.write("\2\2\2\u00cb\u00c9\3\2\2\2\u00cc\u00ce\5\23\n\2\u00cd")
        buf.write("\u00cf\13\2\2\2\u00ce\u00cd\3\2\2\2\u00cf\u00d0\3\2\2")
        buf.write("\2\u00d0\u00d1\3\2\2\2\u00d0\u00ce\3\2\2\2\u00d1\u00d2")
        buf.write("\3\2\2\2\u00d2\u00d3\5\27\f\2\u00d3\60\3\2\2\2\16\2\u0084")
        buf.write("\u008a\u00a0\u00a5\u00a9\u00b2\u00b9\u00c0\u00c7\u00c9")
        buf.write("\u00d0\3\b\2\2")
        return buf.getvalue()


class WedflowLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    ANNOTATION_WED_ATTRIBUTES = 1
    ANNOTATION_WED_CONDITIONS = 2
    ANNOTATION_WED_TRANSITIONS = 3
    ANNOTATION_WED_TRIGGERS = 4
    ANNOTATION_WED_FLOW = 5
    ANNOTATION_AWIC = 6
    SET_NAME = 7
    VALID_NAME = 8
    COLON = 9
    EQUALS = 10
    SEMICOLON = 11
    COMMA = 12
    OPEN_PARENTHESES = 13
    CLOSE_PARENTHESES = 14
    OPEN_CURLY_BRACKETS = 15
    CLOSE_CURLY_BRACKETS = 16
    LINE_COMMENT = 17
    SPACE = 18
    LINEBREAK = 19
    WS = 20
    DEFINITION = 21

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "':'", "'='", "';'", "','", "'('", "')'", "'{'", "'}'", "' '" ]

    symbolicNames = [ "<INVALID>",
            "ANNOTATION_WED_ATTRIBUTES", "ANNOTATION_WED_CONDITIONS", "ANNOTATION_WED_TRANSITIONS", 
            "ANNOTATION_WED_TRIGGERS", "ANNOTATION_WED_FLOW", "ANNOTATION_AWIC", 
            "SET_NAME", "VALID_NAME", "COLON", "EQUALS", "SEMICOLON", "COMMA", 
            "OPEN_PARENTHESES", "CLOSE_PARENTHESES", "OPEN_CURLY_BRACKETS", 
            "CLOSE_CURLY_BRACKETS", "LINE_COMMENT", "SPACE", "LINEBREAK", 
            "WS", "DEFINITION" ]

    ruleNames = [ "ANNOTATION_WED_ATTRIBUTES", "ANNOTATION_WED_CONDITIONS", 
                  "ANNOTATION_WED_TRANSITIONS", "ANNOTATION_WED_TRIGGERS", 
                  "ANNOTATION_WED_FLOW", "ANNOTATION_AWIC", "SET_NAME", 
                  "VALID_NAME", "COLON", "EQUALS", "SEMICOLON", "COMMA", 
                  "OPEN_PARENTHESES", "CLOSE_PARENTHESES", "OPEN_CURLY_BRACKETS", 
                  "CLOSE_CURLY_BRACKETS", "LINE_COMMENT", "NON_SPECIAL", 
                  "DIGIT", "SPACE", "LINEBREAK", "WS", "DEFINITION" ]

    grammarFileName = "WedflowLexer.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


