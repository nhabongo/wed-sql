# Generated from Wedflow.g4 by ANTLR 4.7.1
# encoding: utf-8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\27")
        buf.write("\u00ee\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\4\21\t\21\3\2\3\2\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3.\n\3\3\4\3\4\3\4\3\4\3")
        buf.write("\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write("\3\4\3\4\3\4\5\4E\n\4\3\5\3\5\7\5I\n\5\f\5\16\5L\13\5")
        buf.write("\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\7\6X\n\6\f\6")
        buf.write("\16\6[\13\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\7")
        buf.write("\7g\n\7\f\7\16\7j\13\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7")
        buf.write("\3\7\3\7\3\7\3\7\3\7\7\7y\n\7\f\7\16\7|\13\7\3\b\3\b\7")
        buf.write("\b\u0080\n\b\f\b\16\b\u0083\13\b\3\b\3\b\3\b\3\b\3\b\3")
        buf.write("\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\7\b\u0092\n\b\f\b\16\b")
        buf.write("\u0095\13\b\3\t\3\t\7\t\u0099\n\t\f\t\16\t\u009c\13\t")
        buf.write("\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\7")
        buf.write("\t\u00ab\n\t\f\t\16\t\u00ae\13\t\3\n\3\n\7\n\u00b2\n\n")
        buf.write("\f\n\16\n\u00b5\13\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3")
        buf.write("\13\3\13\3\13\3\13\3\13\3\13\3\13\7\13\u00c6\n\13\f\13")
        buf.write("\16\13\u00c9\13\13\3\13\3\13\3\13\5\13\u00ce\n\13\3\13")
        buf.write("\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f")
        buf.write("\3\f\3\f\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\7")
        buf.write("\21\u00e9\n\21\f\21\16\21\u00ec\13\21\3\21\2\2\22\2\4")
        buf.write("\6\b\n\f\16\20\22\24\26\30\32\34\36 \2\4\3\2\t\n\3\2\24")
        buf.write("\25\2\u00ef\2\"\3\2\2\2\4-\3\2\2\2\6D\3\2\2\2\bF\3\2\2")
        buf.write("\2\nU\3\2\2\2\fd\3\2\2\2\16}\3\2\2\2\20\u0096\3\2\2\2")
        buf.write("\22\u00af\3\2\2\2\24\u00be\3\2\2\2\26\u00d1\3\2\2\2\30")
        buf.write("\u00df\3\2\2\2\32\u00e1\3\2\2\2\34\u00e3\3\2\2\2\36\u00e5")
        buf.write("\3\2\2\2 \u00ea\3\2\2\2\"#\5\4\3\2#\3\3\2\2\2$%\5 \21")
        buf.write("\2%&\5\6\4\2&\'\5 \21\2\'(\5\4\3\2(.\3\2\2\2)*\5 \21\2")
        buf.write("*+\5\6\4\2+,\5 \21\2,.\3\2\2\2-$\3\2\2\2-)\3\2\2\2.\5")
        buf.write("\3\2\2\2/\60\5\b\5\2\60\61\5 \21\2\61\62\7\r\2\2\62E\3")
        buf.write("\2\2\2\63\64\5\n\6\2\64\65\5 \21\2\65\66\7\r\2\2\66E\3")
        buf.write("\2\2\2\678\5\f\7\289\5 \21\29E\3\2\2\2:;\5\16\b\2;<\5")
        buf.write(" \21\2<E\3\2\2\2=>\5\20\t\2>?\5 \21\2?E\3\2\2\2@A\5\22")
        buf.write("\n\2AB\5 \21\2BC\7\r\2\2CE\3\2\2\2D/\3\2\2\2D\63\3\2\2")
        buf.write("\2D\67\3\2\2\2D:\3\2\2\2D=\3\2\2\2D@\3\2\2\2E\7\3\2\2")
        buf.write("\2FJ\7\7\2\2GI\7\24\2\2HG\3\2\2\2IL\3\2\2\2JH\3\2\2\2")
        buf.write("JK\3\2\2\2KM\3\2\2\2LJ\3\2\2\2MN\7\25\2\2NO\5 \21\2OP")
        buf.write("\5\36\20\2PQ\5 \21\2QR\7\f\2\2RS\5 \21\2ST\5\26\f\2T\t")
        buf.write("\3\2\2\2UY\7\3\2\2VX\7\24\2\2WV\3\2\2\2X[\3\2\2\2YW\3")
        buf.write("\2\2\2YZ\3\2\2\2Z\\\3\2\2\2[Y\3\2\2\2\\]\7\25\2\2]^\5")
        buf.write(" \21\2^_\7\t\2\2_`\5 \21\2`a\7\f\2\2ab\5 \21\2bc\5\24")
        buf.write("\13\2c\13\3\2\2\2dh\7\4\2\2eg\7\24\2\2fe\3\2\2\2gj\3\2")
        buf.write("\2\2hf\3\2\2\2hi\3\2\2\2ik\3\2\2\2jh\3\2\2\2kl\7\25\2")
        buf.write("\2lm\5 \21\2mn\7\t\2\2no\5 \21\2op\7\f\2\2pq\5 \21\2q")
        buf.write("r\5\24\13\2rs\5 \21\2st\7\r\2\2tz\5 \21\2uv\5\30\r\2v")
        buf.write("w\5 \21\2wy\3\2\2\2xu\3\2\2\2y|\3\2\2\2zx\3\2\2\2z{\3")
        buf.write("\2\2\2{\r\3\2\2\2|z\3\2\2\2}\u0081\7\5\2\2~\u0080\7\24")
        buf.write("\2\2\177~\3\2\2\2\u0080\u0083\3\2\2\2\u0081\177\3\2\2")
        buf.write("\2\u0081\u0082\3\2\2\2\u0082\u0084\3\2\2\2\u0083\u0081")
        buf.write("\3\2\2\2\u0084\u0085\7\25\2\2\u0085\u0086\5 \21\2\u0086")
        buf.write("\u0087\7\t\2\2\u0087\u0088\5 \21\2\u0088\u0089\7\f\2\2")
        buf.write("\u0089\u008a\5 \21\2\u008a\u008b\5\24\13\2\u008b\u008c")
        buf.write("\5 \21\2\u008c\u008d\7\r\2\2\u008d\u0093\5 \21\2\u008e")
        buf.write("\u008f\5\32\16\2\u008f\u0090\5 \21\2\u0090\u0092\3\2\2")
        buf.write("\2\u0091\u008e\3\2\2\2\u0092\u0095\3\2\2\2\u0093\u0091")
        buf.write("\3\2\2\2\u0093\u0094\3\2\2\2\u0094\17\3\2\2\2\u0095\u0093")
        buf.write("\3\2\2\2\u0096\u009a\7\6\2\2\u0097\u0099\7\24\2\2\u0098")
        buf.write("\u0097\3\2\2\2\u0099\u009c\3\2\2\2\u009a\u0098\3\2\2\2")
        buf.write("\u009a\u009b\3\2\2\2\u009b\u009d\3\2\2\2\u009c\u009a\3")
        buf.write("\2\2\2\u009d\u009e\7\25\2\2\u009e\u009f\5 \21\2\u009f")
        buf.write("\u00a0\7\t\2\2\u00a0\u00a1\5 \21\2\u00a1\u00a2\7\f\2\2")
        buf.write("\u00a2\u00a3\5 \21\2\u00a3\u00a4\5\24\13\2\u00a4\u00a5")
        buf.write("\5 \21\2\u00a5\u00a6\7\r\2\2\u00a6\u00ac\5 \21\2\u00a7")
        buf.write("\u00a8\5\34\17\2\u00a8\u00a9\5 \21\2\u00a9\u00ab\3\2\2")
        buf.write("\2\u00aa\u00a7\3\2\2\2\u00ab\u00ae\3\2\2\2\u00ac\u00aa")
        buf.write("\3\2\2\2\u00ac\u00ad\3\2\2\2\u00ad\21\3\2\2\2\u00ae\u00ac")
        buf.write("\3\2\2\2\u00af\u00b3\7\b\2\2\u00b0\u00b2\7\24\2\2\u00b1")
        buf.write("\u00b0\3\2\2\2\u00b2\u00b5\3\2\2\2\u00b3\u00b1\3\2\2\2")
        buf.write("\u00b3\u00b4\3\2\2\2\u00b4\u00b6\3\2\2\2\u00b5\u00b3\3")
        buf.write("\2\2\2\u00b6\u00b7\7\25\2\2\u00b7\u00b8\5 \21\2\u00b8")
        buf.write("\u00b9\7\t\2\2\u00b9\u00ba\5 \21\2\u00ba\u00bb\7\f\2\2")
        buf.write("\u00bb\u00bc\5 \21\2\u00bc\u00bd\5\24\13\2\u00bd\23\3")
        buf.write("\2\2\2\u00be\u00bf\7\21\2\2\u00bf\u00cd\5 \21\2\u00c0")
        buf.write("\u00c1\5\36\20\2\u00c1\u00c2\5 \21\2\u00c2\u00c3\7\16")
        buf.write("\2\2\u00c3\u00c4\5 \21\2\u00c4\u00c6\3\2\2\2\u00c5\u00c0")
        buf.write("\3\2\2\2\u00c6\u00c9\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7")
        buf.write("\u00c8\3\2\2\2\u00c8\u00ca\3\2\2\2\u00c9\u00c7\3\2\2\2")
        buf.write("\u00ca\u00cb\5\36\20\2\u00cb\u00cc\5 \21\2\u00cc\u00ce")
        buf.write("\3\2\2\2\u00cd\u00c7\3\2\2\2\u00cd\u00ce\3\2\2\2\u00ce")
        buf.write("\u00cf\3\2\2\2\u00cf\u00d0\7\22\2\2\u00d0\25\3\2\2\2\u00d1")
        buf.write("\u00d2\7\21\2\2\u00d2\u00d3\5 \21\2\u00d3\u00d4\7\t\2")
        buf.write("\2\u00d4\u00d5\5 \21\2\u00d5\u00d6\7\16\2\2\u00d6\u00d7")
        buf.write("\5 \21\2\u00d7\u00d8\5\36\20\2\u00d8\u00d9\5 \21\2\u00d9")
        buf.write("\u00da\7\16\2\2\u00da\u00db\5 \21\2\u00db\u00dc\5\36\20")
        buf.write("\2\u00dc\u00dd\5 \21\2\u00dd\u00de\7\22\2\2\u00de\27\3")
        buf.write("\2\2\2\u00df\u00e0\7\27\2\2\u00e0\31\3\2\2\2\u00e1\u00e2")
        buf.write("\7\27\2\2\u00e2\33\3\2\2\2\u00e3\u00e4\7\27\2\2\u00e4")
        buf.write("\35\3\2\2\2\u00e5\u00e6\t\2\2\2\u00e6\37\3\2\2\2\u00e7")
        buf.write("\u00e9\t\3\2\2\u00e8\u00e7\3\2\2\2\u00e9\u00ec\3\2\2\2")
        buf.write("\u00ea\u00e8\3\2\2\2\u00ea\u00eb\3\2\2\2\u00eb!\3\2\2")
        buf.write("\2\u00ec\u00ea\3\2\2\2\20-DJYhz\u0081\u0093\u009a\u00ac")
        buf.write("\u00b3\u00c7\u00cd\u00ea")
        return buf.getvalue()


class Wedflow ( Parser ):

    grammarFileName = "Wedflow.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                     "<INVALID>", "':'", "'='", "';'", "','", "'('", "')'", 
                     "'{'", "'}'", "<INVALID>", "' '" ]

    symbolicNames = [ "<INVALID>", "ANNOTATION_WED_ATTRIBUTES", "ANNOTATION_WED_CONDITIONS", 
                      "ANNOTATION_WED_TRANSITIONS", "ANNOTATION_WED_TRIGGERS", 
                      "ANNOTATION_WED_FLOW", "ANNOTATION_AWIC", "SET_NAME", 
                      "VALID_NAME", "COLON", "EQUALS", "SEMICOLON", "COMMA", 
                      "OPEN_PARENTHESES", "CLOSE_PARENTHESES", "OPEN_CURLY_BRACKETS", 
                      "CLOSE_CURLY_BRACKETS", "LINE_COMMENT", "SPACE", "LINEBREAK", 
                      "WS", "DEFINITION" ]

    RULE_wflow = 0
    RULE_stmt_list = 1
    RULE_stmt = 2
    RULE_create_flow_stmt = 3
    RULE_create_attr_stmt = 4
    RULE_create_cond_stmt = 5
    RULE_create_trans_stmt = 6
    RULE_create_trig_stmt = 7
    RULE_set_fstate_stmt = 8
    RULE_set_expr = 9
    RULE_wed_flow_set_expr = 10
    RULE_condition_def = 11
    RULE_transition_def = 12
    RULE_trigger_def = 13
    RULE_valid_name = 14
    RULE_empty_space = 15

    ruleNames =  [ "wflow", "stmt_list", "stmt", "create_flow_stmt", "create_attr_stmt", 
                   "create_cond_stmt", "create_trans_stmt", "create_trig_stmt", 
                   "set_fstate_stmt", "set_expr", "wed_flow_set_expr", "condition_def", 
                   "transition_def", "trigger_def", "valid_name", "empty_space" ]

    EOF = Token.EOF
    ANNOTATION_WED_ATTRIBUTES=1
    ANNOTATION_WED_CONDITIONS=2
    ANNOTATION_WED_TRANSITIONS=3
    ANNOTATION_WED_TRIGGERS=4
    ANNOTATION_WED_FLOW=5
    ANNOTATION_AWIC=6
    SET_NAME=7
    VALID_NAME=8
    COLON=9
    EQUALS=10
    SEMICOLON=11
    COMMA=12
    OPEN_PARENTHESES=13
    CLOSE_PARENTHESES=14
    OPEN_CURLY_BRACKETS=15
    CLOSE_CURLY_BRACKETS=16
    LINE_COMMENT=17
    SPACE=18
    LINEBREAK=19
    WS=20
    DEFINITION=21

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class WflowContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def stmt_list(self):
            return self.getTypedRuleContext(Wedflow.Stmt_listContext,0)


        def getRuleIndex(self):
            return Wedflow.RULE_wflow

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWflow" ):
                listener.enterWflow(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWflow" ):
                listener.exitWflow(self)




    def wflow(self):

        localctx = Wedflow.WflowContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_wflow)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 32
            self.stmt_list()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Stmt_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def stmt(self):
            return self.getTypedRuleContext(Wedflow.StmtContext,0)


        def stmt_list(self):
            return self.getTypedRuleContext(Wedflow.Stmt_listContext,0)


        def getRuleIndex(self):
            return Wedflow.RULE_stmt_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStmt_list" ):
                listener.enterStmt_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStmt_list" ):
                listener.exitStmt_list(self)




    def stmt_list(self):

        localctx = Wedflow.Stmt_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_stmt_list)
        try:
            self.state = 43
            self._errHandler.sync(self)
            la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 34
                self.empty_space()
                self.state = 35
                self.stmt()
                self.state = 36
                self.empty_space()
                self.state = 37
                self.stmt_list()
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 39
                self.empty_space()
                self.state = 40
                self.stmt()
                self.state = 41
                self.empty_space()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def create_flow_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_flow_stmtContext,0)


        def empty_space(self):
            return self.getTypedRuleContext(Wedflow.Empty_spaceContext,0)


        def SEMICOLON(self):
            return self.getToken(Wedflow.SEMICOLON, 0)

        def create_attr_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_attr_stmtContext,0)


        def create_cond_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_cond_stmtContext,0)


        def create_trans_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_trans_stmtContext,0)


        def create_trig_stmt(self):
            return self.getTypedRuleContext(Wedflow.Create_trig_stmtContext,0)


        def set_fstate_stmt(self):
            return self.getTypedRuleContext(Wedflow.Set_fstate_stmtContext,0)


        def getRuleIndex(self):
            return Wedflow.RULE_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterStmt" ):
                listener.enterStmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitStmt" ):
                listener.exitStmt(self)




    def stmt(self):

        localctx = Wedflow.StmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_stmt)
        try:
            self.state = 66
            self._errHandler.sync(self)
            token = self._input.LA(1)
            if token in [Wedflow.ANNOTATION_WED_FLOW]:
                self.enterOuterAlt(localctx, 1)
                self.state = 45
                self.create_flow_stmt()
                self.state = 46
                self.empty_space()
                self.state = 47
                self.match(Wedflow.SEMICOLON)
                pass
            elif token in [Wedflow.ANNOTATION_WED_ATTRIBUTES]:
                self.enterOuterAlt(localctx, 2)
                self.state = 49
                self.create_attr_stmt()
                self.state = 50
                self.empty_space()
                self.state = 51
                self.match(Wedflow.SEMICOLON)
                pass
            elif token in [Wedflow.ANNOTATION_WED_CONDITIONS]:
                self.enterOuterAlt(localctx, 3)
                self.state = 53
                self.create_cond_stmt()
                self.state = 54
                self.empty_space()
                pass
            elif token in [Wedflow.ANNOTATION_WED_TRANSITIONS]:
                self.enterOuterAlt(localctx, 4)
                self.state = 56
                self.create_trans_stmt()
                self.state = 57
                self.empty_space()
                pass
            elif token in [Wedflow.ANNOTATION_WED_TRIGGERS]:
                self.enterOuterAlt(localctx, 5)
                self.state = 59
                self.create_trig_stmt()
                self.state = 60
                self.empty_space()
                pass
            elif token in [Wedflow.ANNOTATION_AWIC]:
                self.enterOuterAlt(localctx, 6)
                self.state = 62
                self.set_fstate_stmt()
                self.state = 63
                self.empty_space()
                self.state = 64
                self.match(Wedflow.SEMICOLON)
                pass
            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_flow_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ANNOTATION_WED_FLOW(self):
            return self.getToken(Wedflow.ANNOTATION_WED_FLOW, 0)

        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def valid_name(self):
            return self.getTypedRuleContext(Wedflow.Valid_nameContext,0)


        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def wed_flow_set_expr(self):
            return self.getTypedRuleContext(Wedflow.Wed_flow_set_exprContext,0)


        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def getRuleIndex(self):
            return Wedflow.RULE_create_flow_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_flow_stmt" ):
                listener.enterCreate_flow_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_flow_stmt" ):
                listener.exitCreate_flow_stmt(self)




    def create_flow_stmt(self):

        localctx = Wedflow.Create_flow_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_create_flow_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 68
            self.match(Wedflow.ANNOTATION_WED_FLOW)
            self.state = 72
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 69
                self.match(Wedflow.SPACE)
                self.state = 74
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 75
            self.match(Wedflow.LINEBREAK)
            self.state = 76
            self.empty_space()
            self.state = 77
            self.valid_name()
            self.state = 78
            self.empty_space()
            self.state = 79
            self.match(Wedflow.EQUALS)
            self.state = 80
            self.empty_space()
            self.state = 81
            self.wed_flow_set_expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_attr_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ANNOTATION_WED_ATTRIBUTES(self):
            return self.getToken(Wedflow.ANNOTATION_WED_ATTRIBUTES, 0)

        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def set_expr(self):
            return self.getTypedRuleContext(Wedflow.Set_exprContext,0)


        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def getRuleIndex(self):
            return Wedflow.RULE_create_attr_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_attr_stmt" ):
                listener.enterCreate_attr_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_attr_stmt" ):
                listener.exitCreate_attr_stmt(self)




    def create_attr_stmt(self):

        localctx = Wedflow.Create_attr_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_create_attr_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 83
            self.match(Wedflow.ANNOTATION_WED_ATTRIBUTES)
            self.state = 87
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 84
                self.match(Wedflow.SPACE)
                self.state = 89
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 90
            self.match(Wedflow.LINEBREAK)
            self.state = 91
            self.empty_space()
            self.state = 92
            self.match(Wedflow.SET_NAME)
            self.state = 93
            self.empty_space()
            self.state = 94
            self.match(Wedflow.EQUALS)
            self.state = 95
            self.empty_space()
            self.state = 96
            self.set_expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_cond_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ANNOTATION_WED_CONDITIONS(self):
            return self.getToken(Wedflow.ANNOTATION_WED_CONDITIONS, 0)

        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def set_expr(self):
            return self.getTypedRuleContext(Wedflow.Set_exprContext,0)


        def SEMICOLON(self):
            return self.getToken(Wedflow.SEMICOLON, 0)

        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def condition_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Condition_defContext)
            else:
                return self.getTypedRuleContext(Wedflow.Condition_defContext,i)


        def getRuleIndex(self):
            return Wedflow.RULE_create_cond_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_cond_stmt" ):
                listener.enterCreate_cond_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_cond_stmt" ):
                listener.exitCreate_cond_stmt(self)




    def create_cond_stmt(self):

        localctx = Wedflow.Create_cond_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_create_cond_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 98
            self.match(Wedflow.ANNOTATION_WED_CONDITIONS)
            self.state = 102
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 99
                self.match(Wedflow.SPACE)
                self.state = 104
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 105
            self.match(Wedflow.LINEBREAK)
            self.state = 106
            self.empty_space()
            self.state = 107
            self.match(Wedflow.SET_NAME)
            self.state = 108
            self.empty_space()
            self.state = 109
            self.match(Wedflow.EQUALS)
            self.state = 110
            self.empty_space()
            self.state = 111
            self.set_expr()
            self.state = 112
            self.empty_space()
            self.state = 113
            self.match(Wedflow.SEMICOLON)
            self.state = 114
            self.empty_space()
            self.state = 120
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.DEFINITION:
                self.state = 115
                self.condition_def()
                self.state = 116
                self.empty_space()
                self.state = 122
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_trans_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ANNOTATION_WED_TRANSITIONS(self):
            return self.getToken(Wedflow.ANNOTATION_WED_TRANSITIONS, 0)

        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def set_expr(self):
            return self.getTypedRuleContext(Wedflow.Set_exprContext,0)


        def SEMICOLON(self):
            return self.getToken(Wedflow.SEMICOLON, 0)

        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def transition_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Transition_defContext)
            else:
                return self.getTypedRuleContext(Wedflow.Transition_defContext,i)


        def getRuleIndex(self):
            return Wedflow.RULE_create_trans_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_trans_stmt" ):
                listener.enterCreate_trans_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_trans_stmt" ):
                listener.exitCreate_trans_stmt(self)




    def create_trans_stmt(self):

        localctx = Wedflow.Create_trans_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_create_trans_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 123
            self.match(Wedflow.ANNOTATION_WED_TRANSITIONS)
            self.state = 127
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 124
                self.match(Wedflow.SPACE)
                self.state = 129
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 130
            self.match(Wedflow.LINEBREAK)
            self.state = 131
            self.empty_space()
            self.state = 132
            self.match(Wedflow.SET_NAME)
            self.state = 133
            self.empty_space()
            self.state = 134
            self.match(Wedflow.EQUALS)
            self.state = 135
            self.empty_space()
            self.state = 136
            self.set_expr()
            self.state = 137
            self.empty_space()
            self.state = 138
            self.match(Wedflow.SEMICOLON)
            self.state = 139
            self.empty_space()
            self.state = 145
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.DEFINITION:
                self.state = 140
                self.transition_def()
                self.state = 141
                self.empty_space()
                self.state = 147
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Create_trig_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ANNOTATION_WED_TRIGGERS(self):
            return self.getToken(Wedflow.ANNOTATION_WED_TRIGGERS, 0)

        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def set_expr(self):
            return self.getTypedRuleContext(Wedflow.Set_exprContext,0)


        def SEMICOLON(self):
            return self.getToken(Wedflow.SEMICOLON, 0)

        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def trigger_def(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Trigger_defContext)
            else:
                return self.getTypedRuleContext(Wedflow.Trigger_defContext,i)


        def getRuleIndex(self):
            return Wedflow.RULE_create_trig_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCreate_trig_stmt" ):
                listener.enterCreate_trig_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCreate_trig_stmt" ):
                listener.exitCreate_trig_stmt(self)




    def create_trig_stmt(self):

        localctx = Wedflow.Create_trig_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_create_trig_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 148
            self.match(Wedflow.ANNOTATION_WED_TRIGGERS)
            self.state = 152
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 149
                self.match(Wedflow.SPACE)
                self.state = 154
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 155
            self.match(Wedflow.LINEBREAK)
            self.state = 156
            self.empty_space()
            self.state = 157
            self.match(Wedflow.SET_NAME)
            self.state = 158
            self.empty_space()
            self.state = 159
            self.match(Wedflow.EQUALS)
            self.state = 160
            self.empty_space()
            self.state = 161
            self.set_expr()
            self.state = 162
            self.empty_space()
            self.state = 163
            self.match(Wedflow.SEMICOLON)
            self.state = 164
            self.empty_space()
            self.state = 170
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.DEFINITION:
                self.state = 165
                self.trigger_def()
                self.state = 166
                self.empty_space()
                self.state = 172
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Set_fstate_stmtContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def ANNOTATION_AWIC(self):
            return self.getToken(Wedflow.ANNOTATION_AWIC, 0)

        def LINEBREAK(self):
            return self.getToken(Wedflow.LINEBREAK, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def EQUALS(self):
            return self.getToken(Wedflow.EQUALS, 0)

        def set_expr(self):
            return self.getTypedRuleContext(Wedflow.Set_exprContext,0)


        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def getRuleIndex(self):
            return Wedflow.RULE_set_fstate_stmt

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSet_fstate_stmt" ):
                listener.enterSet_fstate_stmt(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSet_fstate_stmt" ):
                listener.exitSet_fstate_stmt(self)




    def set_fstate_stmt(self):

        localctx = Wedflow.Set_fstate_stmtContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_set_fstate_stmt)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 173
            self.match(Wedflow.ANNOTATION_AWIC)
            self.state = 177
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==Wedflow.SPACE:
                self.state = 174
                self.match(Wedflow.SPACE)
                self.state = 179
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 180
            self.match(Wedflow.LINEBREAK)
            self.state = 181
            self.empty_space()
            self.state = 182
            self.match(Wedflow.SET_NAME)
            self.state = 183
            self.empty_space()
            self.state = 184
            self.match(Wedflow.EQUALS)
            self.state = 185
            self.empty_space()
            self.state = 186
            self.set_expr()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Set_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OPEN_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.OPEN_CURLY_BRACKETS, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def CLOSE_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.CLOSE_CURLY_BRACKETS, 0)

        def valid_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Valid_nameContext)
            else:
                return self.getTypedRuleContext(Wedflow.Valid_nameContext,i)


        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.COMMA)
            else:
                return self.getToken(Wedflow.COMMA, i)

        def getRuleIndex(self):
            return Wedflow.RULE_set_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSet_expr" ):
                listener.enterSet_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSet_expr" ):
                listener.exitSet_expr(self)




    def set_expr(self):

        localctx = Wedflow.Set_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_set_expr)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 188
            self.match(Wedflow.OPEN_CURLY_BRACKETS)
            self.state = 189
            self.empty_space()
            self.state = 203
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            if _la==Wedflow.SET_NAME or _la==Wedflow.VALID_NAME:
                self.state = 197
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,11,self._ctx)
                while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                    if _alt==1:
                        self.state = 190
                        self.valid_name()
                        self.state = 191
                        self.empty_space()
                        self.state = 192
                        self.match(Wedflow.COMMA)
                        self.state = 193
                        self.empty_space() 
                    self.state = 199
                    self._errHandler.sync(self)
                    _alt = self._interp.adaptivePredict(self._input,11,self._ctx)

                self.state = 200
                self.valid_name()
                self.state = 201
                self.empty_space()


            self.state = 205
            self.match(Wedflow.CLOSE_CURLY_BRACKETS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Wed_flow_set_exprContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def OPEN_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.OPEN_CURLY_BRACKETS, 0)

        def empty_space(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Empty_spaceContext)
            else:
                return self.getTypedRuleContext(Wedflow.Empty_spaceContext,i)


        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def COMMA(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.COMMA)
            else:
                return self.getToken(Wedflow.COMMA, i)

        def valid_name(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(Wedflow.Valid_nameContext)
            else:
                return self.getTypedRuleContext(Wedflow.Valid_nameContext,i)


        def CLOSE_CURLY_BRACKETS(self):
            return self.getToken(Wedflow.CLOSE_CURLY_BRACKETS, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_wed_flow_set_expr

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterWed_flow_set_expr" ):
                listener.enterWed_flow_set_expr(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitWed_flow_set_expr" ):
                listener.exitWed_flow_set_expr(self)




    def wed_flow_set_expr(self):

        localctx = Wedflow.Wed_flow_set_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_wed_flow_set_expr)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 207
            self.match(Wedflow.OPEN_CURLY_BRACKETS)
            self.state = 208
            self.empty_space()
            self.state = 209
            self.match(Wedflow.SET_NAME)
            self.state = 210
            self.empty_space()
            self.state = 211
            self.match(Wedflow.COMMA)
            self.state = 212
            self.empty_space()
            self.state = 213
            self.valid_name()
            self.state = 214
            self.empty_space()
            self.state = 215
            self.match(Wedflow.COMMA)
            self.state = 216
            self.empty_space()
            self.state = 217
            self.valid_name()
            self.state = 218
            self.empty_space()
            self.state = 219
            self.match(Wedflow.CLOSE_CURLY_BRACKETS)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Condition_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DEFINITION(self):
            return self.getToken(Wedflow.DEFINITION, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_condition_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCondition_def" ):
                listener.enterCondition_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCondition_def" ):
                listener.exitCondition_def(self)




    def condition_def(self):

        localctx = Wedflow.Condition_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_condition_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 221
            self.match(Wedflow.DEFINITION)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Transition_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DEFINITION(self):
            return self.getToken(Wedflow.DEFINITION, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_transition_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTransition_def" ):
                listener.enterTransition_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTransition_def" ):
                listener.exitTransition_def(self)




    def transition_def(self):

        localctx = Wedflow.Transition_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_transition_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 223
            self.match(Wedflow.DEFINITION)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Trigger_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def DEFINITION(self):
            return self.getToken(Wedflow.DEFINITION, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_trigger_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTrigger_def" ):
                listener.enterTrigger_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTrigger_def" ):
                listener.exitTrigger_def(self)




    def trigger_def(self):

        localctx = Wedflow.Trigger_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_trigger_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 225
            self.match(Wedflow.DEFINITION)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Valid_nameContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SET_NAME(self):
            return self.getToken(Wedflow.SET_NAME, 0)

        def VALID_NAME(self):
            return self.getToken(Wedflow.VALID_NAME, 0)

        def getRuleIndex(self):
            return Wedflow.RULE_valid_name

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterValid_name" ):
                listener.enterValid_name(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitValid_name" ):
                listener.exitValid_name(self)




    def valid_name(self):

        localctx = Wedflow.Valid_nameContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_valid_name)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 227
            _la = self._input.LA(1)
            if not(_la==Wedflow.SET_NAME or _la==Wedflow.VALID_NAME):
                self._errHandler.recoverInline(self)
            else:
                self._errHandler.reportMatch(self)
                self.consume()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Empty_spaceContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def SPACE(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.SPACE)
            else:
                return self.getToken(Wedflow.SPACE, i)

        def LINEBREAK(self, i:int=None):
            if i is None:
                return self.getTokens(Wedflow.LINEBREAK)
            else:
                return self.getToken(Wedflow.LINEBREAK, i)

        def getRuleIndex(self):
            return Wedflow.RULE_empty_space

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterEmpty_space" ):
                listener.enterEmpty_space(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitEmpty_space" ):
                listener.exitEmpty_space(self)




    def empty_space(self):

        localctx = Wedflow.Empty_spaceContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_empty_space)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 232
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,13,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    self.state = 229
                    _la = self._input.LA(1)
                    if not(_la==Wedflow.SPACE or _la==Wedflow.LINEBREAK):
                        self._errHandler.recoverInline(self)
                    else:
                        self._errHandler.reportMatch(self)
                        self.consume() 
                self.state = 234
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,13,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





