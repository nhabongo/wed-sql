import sys
from antlr4 import *

class WedflowTranslator():

    def __init__(self):
        self.cond_def = {}

    def create_flow(self, ctx):
        wedflow_name = ctx.valid_name().getText()
        wsql = "create flow {};\n".format(wedflow_name) + "\cw {};\n".format(wedflow_name) + "begin;\n" + "\n"
        return wsql 

    def create_attributes(self, ctx):
        attribute_name_list = ctx.set_expr().valid_name()
        wsql = ""
        for attribute_name in attribute_name_list:
            wsql += "create wed-attribute {};\n".format(attribute_name.getText())
        wsql += "\n"
        return wsql

    def create_conditions(self, ctx):
        condition_name_list = ctx.set_expr().valid_name()
        condition_definition_list = ctx.condition_def()
        for condition_definition in condition_definition_list:
            definition = condition_definition.DEFINITION().getText()
            definition_parts = definition.split(":", 1)
            condition_name = definition_parts[0].strip()
            predicate = definition_parts[1].strip()
            self.cond_def[condition_name] = predicate
            wsql = ""
        for condition_name in condition_name_list:
            wsql += "create wed-condition {} as {}\n".format(condition_name.getText(), self.cond_def[condition_name.getText()])
        wsql += "\n"
        return wsql

    def create_transitions(self, ctx):
        transition_name_list = ctx.set_expr().valid_name()
        wsql = ""
        for transition_name in transition_name_list:
            wsql += "create wed-transition {};\n".format(transition_name.getText())
        wsql += "\n"
        return wsql

    def create_triggers(self, ctx):
        trigger_name_list = ctx.set_expr().valid_name()
        trigger_definition_list = ctx.trigger_def()
        trigger_def = {}
        for trigger_definition in trigger_definition_list:
            definition = trigger_definition.DEFINITION().getText()
            definition_parts = definition.split(":", 1)
            trigger_name = definition_parts[0].strip()
            trigger_expression = definition_parts[1].strip()
            trigger_def[trigger_name] = trigger_expression
        wsql = ""
        for trigger_name in trigger_name_list:
            wsql += "create wed-trigger {} as {}\n".format(trigger_name.getText(), trigger_def[trigger_name.getText()])
        wsql += "\n"
        return wsql

    def set_final_state(self, awic):
        final_state_predicate = ""
        for final_condition in awic:
            final_state_predicate += self.cond_def[final_condition][:-1] + " AND "
        final_state_predicate = final_state_predicate[:-5]
        wsql = ""
        wsql += "set final wed-state as {};\n".format(final_state_predicate)
        wsql += "\n"
        return wsql



