# Generated from WedsqlLexer.g4 by ANTLR 4.7.1
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2=")
        buf.write("\u02a3\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7")
        buf.write("\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write("\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4\23")
        buf.write("\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30")
        buf.write("\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36")
        buf.write("\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%")
        buf.write("\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t,\4-\t-\4.")
        buf.write("\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64")
        buf.write("\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:")
        buf.write("\4;\t;\4<\t<\4=\t=\4>\t>\4?\t?\4@\t@\3\2\6\2\u0083\n\2")
        buf.write("\r\2\16\2\u0084\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3")
        buf.write("\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5")
        buf.write("\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3")
        buf.write("\b\3\b\3\b\3\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\n")
        buf.write("\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3")
        buf.write("\f\3\f\3\f\3\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\17")
        buf.write("\3\17\3\17\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\20\3\20")
        buf.write("\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21")
        buf.write("\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\22\3\22")
        buf.write("\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22\3\22")
        buf.write("\3\22\3\22\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23")
        buf.write("\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24")
        buf.write("\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\25\3\25\3\25\3\25")
        buf.write("\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26\3\26")
        buf.write("\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\27\3\27\3\27")
        buf.write("\3\27\3\27\3\27\3\27\3\27\3\27\3\27\3\30\3\30\3\30\3\30")
        buf.write("\3\30\3\30\3\30\3\30\3\30\3\30\3\31\3\31\3\31\3\31\3\31")
        buf.write("\3\31\3\31\3\31\3\31\3\32\3\32\3\32\3\32\3\32\3\32\3\32")
        buf.write("\3\32\3\32\3\32\3\32\3\32\3\32\3\33\3\33\3\33\3\33\3\33")
        buf.write("\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\33\3\34")
        buf.write("\3\34\3\34\3\34\3\34\3\34\3\35\3\35\3\35\3\35\3\36\3\36")
        buf.write("\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\36\3\37\3\37\3\37")
        buf.write("\3\37\3\37\3\37\3\37\3 \3 \3 \3 \3 \3 \3 \3 \3!\3!\3!")
        buf.write("\3!\3!\3\"\3\"\3\"\3\"\3\"\3\"\3#\3#\3#\3#\3$\3$\3$\3")
        buf.write("$\3$\3$\3$\3$\3$\3$\3%\3%\3%\3%\3%\3&\3&\3&\3&\3&\3&\3")
        buf.write("&\3&\3\'\3\'\3\'\3\'\3\'\3\'\3\'\3(\3(\3(\3(\3(\3(\3)")
        buf.write("\3)\3)\3)\3)\3)\3)\3)\3)\3)\3*\3*\3*\3*\3*\3*\3*\3*\3")
        buf.write("+\3+\3+\3+\3+\3,\3,\3,\3-\3-\3-\3-\3-\3-\3-\3-\3-\3-\3")
        buf.write(".\3.\3.\3.\3.\3.\3.\3.\3.\3.\3/\3/\3\60\3\60\3\61\3\61")
        buf.write("\3\62\3\62\3\63\3\63\3\63\3\64\3\64\3\64\3\64\3\65\3\65")
        buf.write("\3\65\3\66\3\66\3\66\3\66\5\66\u0210\n\66\3\66\7\66\u0213")
        buf.write("\n\66\f\66\16\66\u0216\13\66\3\66\3\66\3\67\3\67\38\3")
        buf.write("8\39\39\79\u0220\n9\f9\169\u0223\139\39\39\39\39\79\u0229")
        buf.write("\n9\f9\169\u022c\139\39\39\59\u0230\n9\3:\5:\u0233\n:")
        buf.write("\3;\6;\u0236\n;\r;\16;\u0237\3<\3<\3=\3=\3=\3=\3=\3=\3")
        buf.write("=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3")
        buf.write("=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\3=\6")
        buf.write("=\u0266\n=\r=\16=\u0267\3>\3>\3>\6>\u026d\n>\r>\16>\u026e")
        buf.write("\3>\3>\3>\6>\u0274\n>\r>\16>\u0275\3>\3>\3>\6>\u027b\n")
        buf.write(">\r>\16>\u027c\3>\6>\u0280\n>\r>\16>\u0281\3>\3>\3?\3")
        buf.write("?\3?\6?\u0289\n?\r?\16?\u028a\3?\3?\3?\6?\u0290\n?\r?")
        buf.write("\16?\u0291\3?\3?\3?\6?\u0297\n?\r?\16?\u0298\3?\6?\u029c")
        buf.write("\n?\r?\16?\u029d\3?\3?\3@\3@\7\u0214\u0221\u022a\u0281")
        buf.write("\u029d\2A\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25")
        buf.write("\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+")
        buf.write("\27-\30/\31\61\32\63\33\65\34\67\359\36;\37= ?!A\"C#E")
        buf.write("$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62c\63e\64g\65i\66k")
        buf.write("\67m\2o\2q8s9u:w\2y\2{;}<\177=\3\2\33\4\2\13\f\17\17\4")
        buf.write("\2EEee\4\2TTtt\4\2GGgg\4\2CCcc\4\2VVvv\4\2YYyy\4\2FFf")
        buf.write("f\4\2HHhh\4\2NNnn\4\2QQqq\4\2OOoo\4\2XXxx\4\2DDdd\4\2")
        buf.write("IIii\4\2KKkk\4\2PPpp\4\2MMmm\4\2ZZzz\4\2RRrr\4\2WWww\4")
        buf.write("\2UUuu\4\2JJjj\6\2//C\\aac|\3\2\62;\2\u02dd\2\3\3\2\2")
        buf.write("\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2")
        buf.write("\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25")
        buf.write("\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3")
        buf.write("\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2")
        buf.write("\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2")
        buf.write("\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\2")
        buf.write("9\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2\2")
        buf.write("\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2")
        buf.write("\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2")
        buf.write("\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2\2\2\2_\3")
        buf.write("\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i")
        buf.write("\3\2\2\2\2k\3\2\2\2\2q\3\2\2\2\2s\3\2\2\2\2u\3\2\2\2\2")
        buf.write("{\3\2\2\2\2}\3\2\2\2\2\177\3\2\2\2\3\u0082\3\2\2\2\5\u0088")
        buf.write("\3\2\2\2\7\u008f\3\2\2\2\t\u0098\3\2\2\2\13\u009d\3\2")
        buf.write("\2\2\r\u00a4\3\2\2\2\17\u00aa\3\2\2\2\21\u00b1\3\2\2\2")
        buf.write("\23\u00b7\3\2\2\2\25\u00c0\3\2\2\2\27\u00c4\3\2\2\2\31")
        buf.write("\u00c8\3\2\2\2\33\u00cc\3\2\2\2\35\u00d0\3\2\2\2\37\u00d4")
        buf.write("\3\2\2\2!\u00e2\3\2\2\2#\u00f0\3\2\2\2%\u00ff\3\2\2\2")
        buf.write("\'\u010b\3\2\2\2)\u011a\3\2\2\2+\u0129\3\2\2\2-\u0139")
        buf.write("\3\2\2\2/\u0146\3\2\2\2\61\u0150\3\2\2\2\63\u0159\3\2")
        buf.write("\2\2\65\u0166\3\2\2\2\67\u0175\3\2\2\29\u017b\3\2\2\2")
        buf.write(";\u017f\3\2\2\2=\u0189\3\2\2\2?\u0190\3\2\2\2A\u0198\3")
        buf.write("\2\2\2C\u019d\3\2\2\2E\u01a3\3\2\2\2G\u01a7\3\2\2\2I\u01b1")
        buf.write("\3\2\2\2K\u01b6\3\2\2\2M\u01be\3\2\2\2O\u01c5\3\2\2\2")
        buf.write("Q\u01cb\3\2\2\2S\u01d5\3\2\2\2U\u01dd\3\2\2\2W\u01e2\3")
        buf.write("\2\2\2Y\u01e5\3\2\2\2[\u01ef\3\2\2\2]\u01f9\3\2\2\2_\u01fb")
        buf.write("\3\2\2\2a\u01fd\3\2\2\2c\u01ff\3\2\2\2e\u0201\3\2\2\2")
        buf.write("g\u0204\3\2\2\2i\u0208\3\2\2\2k\u020f\3\2\2\2m\u0219\3")
        buf.write("\2\2\2o\u021b\3\2\2\2q\u022f\3\2\2\2s\u0232\3\2\2\2u\u0235")
        buf.write("\3\2\2\2w\u0239\3\2\2\2y\u0265\3\2\2\2{\u0269\3\2\2\2")
        buf.write("}\u0285\3\2\2\2\177\u02a1\3\2\2\2\u0081\u0083\t\2\2\2")
        buf.write("\u0082\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084\u0082\3")
        buf.write("\2\2\2\u0084\u0085\3\2\2\2\u0085\u0086\3\2\2\2\u0086\u0087")
        buf.write("\b\2\2\2\u0087\4\3\2\2\2\u0088\u0089\t\3\2\2\u0089\u008a")
        buf.write("\t\4\2\2\u008a\u008b\t\5\2\2\u008b\u008c\t\6\2\2\u008c")
        buf.write("\u008d\t\7\2\2\u008d\u008e\t\5\2\2\u008e\6\3\2\2\2\u008f")
        buf.write("\u0090\t\b\2\2\u0090\u0091\t\5\2\2\u0091\u0092\t\t\2\2")
        buf.write("\u0092\u0093\7/\2\2\u0093\u0094\t\n\2\2\u0094\u0095\t")
        buf.write("\13\2\2\u0095\u0096\t\f\2\2\u0096\u0097\t\b\2\2\u0097")
        buf.write("\b\3\2\2\2\u0098\u0099\t\n\2\2\u0099\u009a\t\13\2\2\u009a")
        buf.write("\u009b\t\f\2\2\u009b\u009c\t\b\2\2\u009c\n\3\2\2\2\u009d")
        buf.write("\u009e\t\4\2\2\u009e\u009f\t\5\2\2\u009f\u00a0\t\r\2\2")
        buf.write("\u00a0\u00a1\t\f\2\2\u00a1\u00a2\t\16\2\2\u00a2\u00a3")
        buf.write("\t\5\2\2\u00a3\f\3\2\2\2\u00a4\u00a5\t\17\2\2\u00a5\u00a6")
        buf.write("\t\5\2\2\u00a6\u00a7\t\20\2\2\u00a7\u00a8\t\21\2\2\u00a8")
        buf.write("\u00a9\t\22\2\2\u00a9\16\3\2\2\2\u00aa\u00ab\t\3\2\2\u00ab")
        buf.write("\u00ac\t\f\2\2\u00ac\u00ad\t\r\2\2\u00ad\u00ae\t\r\2\2")
        buf.write("\u00ae\u00af\t\21\2\2\u00af\u00b0\t\7\2\2\u00b0\20\3\2")
        buf.write("\2\2\u00b1\u00b2\t\6\2\2\u00b2\u00b3\t\17\2\2\u00b3\u00b4")
        buf.write("\t\f\2\2\u00b4\u00b5\t\4\2\2\u00b5\u00b6\t\7\2\2\u00b6")
        buf.write("\22\3\2\2\2\u00b7\u00b8\t\4\2\2\u00b8\u00b9\t\f\2\2\u00b9")
        buf.write("\u00ba\t\13\2\2\u00ba\u00bb\t\13\2\2\u00bb\u00bc\t\17")
        buf.write("\2\2\u00bc\u00bd\t\6\2\2\u00bd\u00be\t\3\2\2\u00be\u00bf")
        buf.write("\t\23\2\2\u00bf\24\3\2\2\2\u00c0\u00c1\7^\2\2\u00c1\u00c2")
        buf.write("\t\3\2\2\u00c2\u00c3\t\b\2\2\u00c3\26\3\2\2\2\u00c4\u00c5")
        buf.write("\7^\2\2\u00c5\u00c6\t\13\2\2\u00c6\u00c7\t\b\2\2\u00c7")
        buf.write("\30\3\2\2\2\u00c8\u00c9\7^\2\2\u00c9\u00ca\t\24\2\2\u00ca")
        buf.write("\u00cb\t\t\2\2\u00cb\32\3\2\2\2\u00cc\u00cd\7^\2\2\u00cd")
        buf.write("\u00ce\t\24\2\2\u00ce\u00cf\t\4\2\2\u00cf\34\3\2\2\2\u00d0")
        buf.write("\u00d1\7^\2\2\u00d1\u00d2\t\f\2\2\u00d2\u00d3\t\25\2\2")
        buf.write("\u00d3\36\3\2\2\2\u00d4\u00d5\t\b\2\2\u00d5\u00d6\t\5")
        buf.write("\2\2\u00d6\u00d7\t\t\2\2\u00d7\u00d8\7/\2\2\u00d8\u00d9")
        buf.write("\t\6\2\2\u00d9\u00da\t\7\2\2\u00da\u00db\t\7\2\2\u00db")
        buf.write("\u00dc\t\4\2\2\u00dc\u00dd\t\21\2\2\u00dd\u00de\t\17\2")
        buf.write("\2\u00de\u00df\t\26\2\2\u00df\u00e0\t\7\2\2\u00e0\u00e1")
        buf.write("\t\5\2\2\u00e1 \3\2\2\2\u00e2\u00e3\t\b\2\2\u00e3\u00e4")
        buf.write("\t\5\2\2\u00e4\u00e5\t\t\2\2\u00e5\u00e6\7/\2\2\u00e6")
        buf.write("\u00e7\t\3\2\2\u00e7\u00e8\t\f\2\2\u00e8\u00e9\t\22\2")
        buf.write("\2\u00e9\u00ea\t\t\2\2\u00ea\u00eb\t\21\2\2\u00eb\u00ec")
        buf.write("\t\7\2\2\u00ec\u00ed\t\21\2\2\u00ed\u00ee\t\f\2\2\u00ee")
        buf.write("\u00ef\t\22\2\2\u00ef\"\3\2\2\2\u00f0\u00f1\t\b\2\2\u00f1")
        buf.write("\u00f2\t\5\2\2\u00f2\u00f3\t\t\2\2\u00f3\u00f4\7/\2\2")
        buf.write("\u00f4\u00f5\t\7\2\2\u00f5\u00f6\t\4\2\2\u00f6\u00f7\t")
        buf.write("\6\2\2\u00f7\u00f8\t\22\2\2\u00f8\u00f9\t\27\2\2\u00f9")
        buf.write("\u00fa\t\21\2\2\u00fa\u00fb\t\7\2\2\u00fb\u00fc\t\21\2")
        buf.write("\2\u00fc\u00fd\t\f\2\2\u00fd\u00fe\t\22\2\2\u00fe$\3\2")
        buf.write("\2\2\u00ff\u0100\t\b\2\2\u0100\u0101\t\5\2\2\u0101\u0102")
        buf.write("\t\t\2\2\u0102\u0103\7/\2\2\u0103\u0104\t\7\2\2\u0104")
        buf.write("\u0105\t\4\2\2\u0105\u0106\t\21\2\2\u0106\u0107\t\20\2")
        buf.write("\2\u0107\u0108\t\20\2\2\u0108\u0109\t\5\2\2\u0109\u010a")
        buf.write("\t\4\2\2\u010a&\3\2\2\2\u010b\u010c\t\b\2\2\u010c\u010d")
        buf.write("\t\5\2\2\u010d\u010e\t\t\2\2\u010e\u010f\7/\2\2\u010f")
        buf.write("\u0110\t\6\2\2\u0110\u0111\t\7\2\2\u0111\u0112\t\7\2\2")
        buf.write("\u0112\u0113\t\4\2\2\u0113\u0114\t\21\2\2\u0114\u0115")
        buf.write("\t\17\2\2\u0115\u0116\t\26\2\2\u0116\u0117\t\7\2\2\u0117")
        buf.write("\u0118\t\5\2\2\u0118\u0119\t\27\2\2\u0119(\3\2\2\2\u011a")
        buf.write("\u011b\t\b\2\2\u011b\u011c\t\5\2\2\u011c\u011d\t\t\2\2")
        buf.write("\u011d\u011e\7/\2\2\u011e\u011f\t\3\2\2\u011f\u0120\t")
        buf.write("\f\2\2\u0120\u0121\t\22\2\2\u0121\u0122\t\t\2\2\u0122")
        buf.write("\u0123\t\21\2\2\u0123\u0124\t\7\2\2\u0124\u0125\t\21\2")
        buf.write("\2\u0125\u0126\t\f\2\2\u0126\u0127\t\22\2\2\u0127\u0128")
        buf.write("\t\27\2\2\u0128*\3\2\2\2\u0129\u012a\t\b\2\2\u012a\u012b")
        buf.write("\t\5\2\2\u012b\u012c\t\t\2\2\u012c\u012d\7/\2\2\u012d")
        buf.write("\u012e\t\7\2\2\u012e\u012f\t\4\2\2\u012f\u0130\t\6\2\2")
        buf.write("\u0130\u0131\t\22\2\2\u0131\u0132\t\27\2\2\u0132\u0133")
        buf.write("\t\21\2\2\u0133\u0134\t\7\2\2\u0134\u0135\t\21\2\2\u0135")
        buf.write("\u0136\t\f\2\2\u0136\u0137\t\22\2\2\u0137\u0138\t\27\2")
        buf.write("\2\u0138,\3\2\2\2\u0139\u013a\t\b\2\2\u013a\u013b\t\5")
        buf.write("\2\2\u013b\u013c\t\t\2\2\u013c\u013d\7/\2\2\u013d\u013e")
        buf.write("\t\7\2\2\u013e\u013f\t\4\2\2\u013f\u0140\t\21\2\2\u0140")
        buf.write("\u0141\t\20\2\2\u0141\u0142\t\20\2\2\u0142\u0143\t\5\2")
        buf.write("\2\u0143\u0144\t\4\2\2\u0144\u0145\t\27\2\2\u0145.\3\2")
        buf.write("\2\2\u0146\u0147\t\21\2\2\u0147\u0148\t\22\2\2\u0148\u0149")
        buf.write("\t\27\2\2\u0149\u014a\t\7\2\2\u014a\u014b\t\6\2\2\u014b")
        buf.write("\u014c\t\22\2\2\u014c\u014d\t\3\2\2\u014d\u014e\t\5\2")
        buf.write("\2\u014e\u014f\t\27\2\2\u014f\60\3\2\2\2\u0150\u0151\t")
        buf.write("\21\2\2\u0151\u0152\t\22\2\2\u0152\u0153\t\27\2\2\u0153")
        buf.write("\u0154\t\7\2\2\u0154\u0155\t\6\2\2\u0155\u0156\t\22\2")
        buf.write("\2\u0156\u0157\t\3\2\2\u0157\u0158\t\5\2\2\u0158\62\3")
        buf.write("\2\2\2\u0159\u015a\t\21\2\2\u015a\u015b\t\22\2\2\u015b")
        buf.write("\u015c\t\3\2\2\u015c\u015d\t\f\2\2\u015d\u015e\t\22\2")
        buf.write("\2\u015e\u015f\t\27\2\2\u015f\u0160\t\21\2\2\u0160\u0161")
        buf.write("\t\27\2\2\u0161\u0162\t\7\2\2\u0162\u0163\t\5\2\2\u0163")
        buf.write("\u0164\t\22\2\2\u0164\u0165\t\7\2\2\u0165\64\3\2\2\2\u0166")
        buf.write("\u0167\t\7\2\2\u0167\u0168\t\4\2\2\u0168\u0169\t\6\2\2")
        buf.write("\u0169\u016a\t\22\2\2\u016a\u016b\t\27\2\2\u016b\u016c")
        buf.write("\t\6\2\2\u016c\u016d\t\3\2\2\u016d\u016e\t\7\2\2\u016e")
        buf.write("\u016f\t\21\2\2\u016f\u0170\t\f\2\2\u0170\u0171\t\22\2")
        buf.write("\2\u0171\u0172\t\21\2\2\u0172\u0173\t\22\2\2\u0173\u0174")
        buf.write("\t\20\2\2\u0174\66\3\2\2\2\u0175\u0176\t\n\2\2\u0176\u0177")
        buf.write("\t\21\2\2\u0177\u0178\t\22\2\2\u0178\u0179\t\6\2\2\u0179")
        buf.write("\u017a\t\13\2\2\u017a8\3\2\2\2\u017b\u017c\t\27\2\2\u017c")
        buf.write("\u017d\t\5\2\2\u017d\u017e\t\7\2\2\u017e:\3\2\2\2\u017f")
        buf.write("\u0180\t\b\2\2\u0180\u0181\t\5\2\2\u0181\u0182\t\t\2\2")
        buf.write("\u0182\u0183\7/\2\2\u0183\u0184\t\27\2\2\u0184\u0185\t")
        buf.write("\7\2\2\u0185\u0186\t\6\2\2\u0186\u0187\t\7\2\2\u0187\u0188")
        buf.write("\t\5\2\2\u0188<\3\2\2\2\u0189\u018a\t\5\2\2\u018a\u018b")
        buf.write("\t\22\2\2\u018b\u018c\t\6\2\2\u018c\u018d\t\17\2\2\u018d")
        buf.write("\u018e\t\13\2\2\u018e\u018f\t\5\2\2\u018f>\3\2\2\2\u0190")
        buf.write("\u0191\t\t\2\2\u0191\u0192\t\21\2\2\u0192\u0193\t\27\2")
        buf.write("\2\u0193\u0194\t\6\2\2\u0194\u0195\t\17\2\2\u0195\u0196")
        buf.write("\t\13\2\2\u0196\u0197\t\5\2\2\u0197@\3\2\2\2\u0198\u0199")
        buf.write("\t\13\2\2\u0199\u019a\t\21\2\2\u019a\u019b\t\27\2\2\u019b")
        buf.write("\u019c\t\7\2\2\u019cB\3\2\2\2\u019d\u019e\t\n\2\2\u019e")
        buf.write("\u019f\t\21\2\2\u019f\u01a0\t\4\2\2\u01a0\u01a1\t\5\2")
        buf.write("\2\u01a1\u01a2\t\t\2\2\u01a2D\3\2\2\2\u01a3\u01a4\t\20")
        buf.write("\2\2\u01a4\u01a5\t\5\2\2\u01a5\u01a6\t\7\2\2\u01a6F\3")
        buf.write("\2\2\2\u01a7\u01a8\t\b\2\2\u01a8\u01a9\t\5\2\2\u01a9\u01aa")
        buf.write("\t\t\2\2\u01aa\u01ab\7/\2\2\u01ab\u01ac\t\7\2\2\u01ac")
        buf.write("\u01ad\t\4\2\2\u01ad\u01ae\t\6\2\2\u01ae\u01af\t\3\2\2")
        buf.write("\u01af\u01b0\t\5\2\2\u01b0H\3\2\2\2\u01b1\u01b2\t\27\2")
        buf.write("\2\u01b2\u01b3\t\30\2\2\u01b3\u01b4\t\f\2\2\u01b4\u01b5")
        buf.write("\t\b\2\2\u01b5J\3\2\2\2\u01b6\u01b7\t\t\2\2\u01b7\u01b8")
        buf.write("\t\5\2\2\u01b8\u01b9\t\n\2\2\u01b9\u01ba\t\6\2\2\u01ba")
        buf.write("\u01bb\t\26\2\2\u01bb\u01bc\t\13\2\2\u01bc\u01bd\t\7\2")
        buf.write("\2\u01bdL\3\2\2\2\u01be\u01bf\t\16\2\2\u01bf\u01c0\t\6")
        buf.write("\2\2\u01c0\u01c1\t\13\2\2\u01c1\u01c2\t\26\2\2\u01c2\u01c3")
        buf.write("\t\5\2\2\u01c3\u01c4\t\27\2\2\u01c4N\3\2\2\2\u01c5\u01c6")
        buf.write("\t\16\2\2\u01c6\u01c7\t\6\2\2\u01c7\u01c8\t\13\2\2\u01c8")
        buf.write("\u01c9\t\26\2\2\u01c9\u01ca\t\5\2\2\u01caP\3\2\2\2\u01cb")
        buf.write("\u01cc\t\3\2\2\u01cc\u01cd\t\f\2\2\u01cd\u01ce\t\22\2")
        buf.write("\2\u01ce\u01cf\t\t\2\2\u01cf\u01d0\t\21\2\2\u01d0\u01d1")
        buf.write("\t\7\2\2\u01d1\u01d2\t\21\2\2\u01d2\u01d3\t\f\2\2\u01d3")
        buf.write("\u01d4\t\22\2\2\u01d4R\3\2\2\2\u01d5\u01d6\t\7\2\2\u01d6")
        buf.write("\u01d7\t\21\2\2\u01d7\u01d8\t\r\2\2\u01d8\u01d9\t\5\2")
        buf.write("\2\u01d9\u01da\t\f\2\2\u01da\u01db\t\26\2\2\u01db\u01dc")
        buf.write("\t\7\2\2\u01dcT\3\2\2\2\u01dd\u01de\t\27\2\2\u01de\u01df")
        buf.write("\t\7\2\2\u01df\u01e0\t\f\2\2\u01e0\u01e1\t\25\2\2\u01e1")
        buf.write("V\3\2\2\2\u01e2\u01e3\t\f\2\2\u01e3\u01e4\t\22\2\2\u01e4")
        buf.write("X\3\2\2\2\u01e5\u01e6\t\7\2\2\u01e6\u01e7\t\5\2\2\u01e7")
        buf.write("\u01e8\t\4\2\2\u01e8\u01e9\t\r\2\2\u01e9\u01ea\t\21\2")
        buf.write("\2\u01ea\u01eb\t\22\2\2\u01eb\u01ec\t\6\2\2\u01ec\u01ed")
        buf.write("\t\7\2\2\u01ed\u01ee\t\5\2\2\u01eeZ\3\2\2\2\u01ef\u01f0")
        buf.write("\t\4\2\2\u01f0\u01f1\t\5\2\2\u01f1\u01f2\t\21\2\2\u01f2")
        buf.write("\u01f3\t\22\2\2\u01f3\u01f4\t\27\2\2\u01f4\u01f5\t\7\2")
        buf.write("\2\u01f5\u01f6\t\6\2\2\u01f6\u01f7\t\7\2\2\u01f7\u01f8")
        buf.write("\t\5\2\2\u01f8\\\3\2\2\2\u01f9\u01fa\7=\2\2\u01fa^\3\2")
        buf.write("\2\2\u01fb\u01fc\7.\2\2\u01fc`\3\2\2\2\u01fd\u01fe\7*")
        buf.write("\2\2\u01feb\3\2\2\2\u01ff\u0200\7+\2\2\u0200d\3\2\2\2")
        buf.write("\u0201\u0202\t\6\2\2\u0202\u0203\t\27\2\2\u0203f\3\2\2")
        buf.write("\2\u0204\u0205\t\n\2\2\u0205\u0206\t\f\2\2\u0206\u0207")
        buf.write("\t\4\2\2\u0207h\3\2\2\2\u0208\u0209\t\27\2\2\u0209\u020a")
        buf.write("\t\25\2\2\u020aj\3\2\2\2\u020b\u020c\7\61\2\2\u020c\u0210")
        buf.write("\7\61\2\2\u020d\u020e\7/\2\2\u020e\u0210\7/\2\2\u020f")
        buf.write("\u020b\3\2\2\2\u020f\u020d\3\2\2\2\u0210\u0214\3\2\2\2")
        buf.write("\u0211\u0213\13\2\2\2\u0212\u0211\3\2\2\2\u0213\u0216")
        buf.write("\3\2\2\2\u0214\u0215\3\2\2\2\u0214\u0212\3\2\2\2\u0215")
        buf.write("\u0217\3\2\2\2\u0216\u0214\3\2\2\2\u0217\u0218\5]/\2\u0218")
        buf.write("l\3\2\2\2\u0219\u021a\7$\2\2\u021an\3\2\2\2\u021b\u021c")
        buf.write("\7)\2\2\u021cp\3\2\2\2\u021d\u0221\5m\67\2\u021e\u0220")
        buf.write("\13\2\2\2\u021f\u021e\3\2\2\2\u0220\u0223\3\2\2\2\u0221")
        buf.write("\u0222\3\2\2\2\u0221\u021f\3\2\2\2\u0222\u0224\3\2\2\2")
        buf.write("\u0223\u0221\3\2\2\2\u0224\u0225\5m\67\2\u0225\u0230\3")
        buf.write("\2\2\2\u0226\u022a\5o8\2\u0227\u0229\13\2\2\2\u0228\u0227")
        buf.write("\3\2\2\2\u0229\u022c\3\2\2\2\u022a\u022b\3\2\2\2\u022a")
        buf.write("\u0228\3\2\2\2\u022b\u022d\3\2\2\2\u022c\u022a\3\2\2\2")
        buf.write("\u022d\u022e\5o8\2\u022e\u0230\3\2\2\2\u022f\u021d\3\2")
        buf.write("\2\2\u022f\u0226\3\2\2\2\u0230r\3\2\2\2\u0231\u0233\t")
        buf.write("\31\2\2\u0232\u0231\3\2\2\2\u0233t\3\2\2\2\u0234\u0236")
        buf.write("\5w<\2\u0235\u0234\3\2\2\2\u0236\u0237\3\2\2\2\u0237\u0235")
        buf.write("\3\2\2\2\u0237\u0238\3\2\2\2\u0238v\3\2\2\2\u0239\u023a")
        buf.write("\t\32\2\2\u023ax\3\2\2\2\u023b\u0266\5s:\2\u023c\u0266")
        buf.write("\5u;\2\u023d\u0266\5\5\3\2\u023e\u0266\5\7\4\2\u023f\u0266")
        buf.write("\5\t\5\2\u0240\u0266\5\13\6\2\u0241\u0266\5\r\7\2\u0242")
        buf.write("\u0266\5\17\b\2\u0243\u0266\5\21\t\2\u0244\u0266\5\23")
        buf.write("\n\2\u0245\u0266\5\37\20\2\u0246\u0266\5!\21\2\u0247\u0266")
        buf.write("\5#\22\2\u0248\u0266\5%\23\2\u0249\u0266\5\'\24\2\u024a")
        buf.write("\u0266\5)\25\2\u024b\u0266\5+\26\2\u024c\u0266\5-\27\2")
        buf.write("\u024d\u0266\5/\30\2\u024e\u0266\5\61\31\2\u024f\u0266")
        buf.write("\5\63\32\2\u0250\u0266\5\65\33\2\u0251\u0266\5\67\34\2")
        buf.write("\u0252\u0266\59\35\2\u0253\u0266\5;\36\2\u0254\u0266\5")
        buf.write("=\37\2\u0255\u0266\5? \2\u0256\u0266\5A!\2\u0257\u0266")
        buf.write("\5C\"\2\u0258\u0266\5E#\2\u0259\u0266\5G$\2\u025a\u0266")
        buf.write("\5I%\2\u025b\u0266\5K&\2\u025c\u0266\5M\'\2\u025d\u0266")
        buf.write("\5O(\2\u025e\u0266\5Q)\2\u025f\u0266\5S*\2\u0260\u0266")
        buf.write("\5U+\2\u0261\u0266\5W,\2\u0262\u0266\5e\63\2\u0263\u0266")
        buf.write("\5g\64\2\u0264\u0266\5i\65\2\u0265\u023b\3\2\2\2\u0265")
        buf.write("\u023c\3\2\2\2\u0265\u023d\3\2\2\2\u0265\u023e\3\2\2\2")
        buf.write("\u0265\u023f\3\2\2\2\u0265\u0240\3\2\2\2\u0265\u0241\3")
        buf.write("\2\2\2\u0265\u0242\3\2\2\2\u0265\u0243\3\2\2\2\u0265\u0244")
        buf.write("\3\2\2\2\u0265\u0245\3\2\2\2\u0265\u0246\3\2\2\2\u0265")
        buf.write("\u0247\3\2\2\2\u0265\u0248\3\2\2\2\u0265\u0249\3\2\2\2")
        buf.write("\u0265\u024a\3\2\2\2\u0265\u024b\3\2\2\2\u0265\u024c\3")
        buf.write("\2\2\2\u0265\u024d\3\2\2\2\u0265\u024e\3\2\2\2\u0265\u024f")
        buf.write("\3\2\2\2\u0265\u0250\3\2\2\2\u0265\u0251\3\2\2\2\u0265")
        buf.write("\u0252\3\2\2\2\u0265\u0253\3\2\2\2\u0265\u0254\3\2\2\2")
        buf.write("\u0265\u0255\3\2\2\2\u0265\u0256\3\2\2\2\u0265\u0257\3")
        buf.write("\2\2\2\u0265\u0258\3\2\2\2\u0265\u0259\3\2\2\2\u0265\u025a")
        buf.write("\3\2\2\2\u0265\u025b\3\2\2\2\u0265\u025c\3\2\2\2\u0265")
        buf.write("\u025d\3\2\2\2\u0265\u025e\3\2\2\2\u0265\u025f\3\2\2\2")
        buf.write("\u0265\u0260\3\2\2\2\u0265\u0261\3\2\2\2\u0265\u0262\3")
        buf.write("\2\2\2\u0265\u0263\3\2\2\2\u0265\u0264\3\2\2\2\u0266\u0267")
        buf.write("\3\2\2\2\u0267\u0265\3\2\2\2\u0267\u0268\3\2\2\2\u0268")
        buf.write("z\3\2\2\2\u0269\u026c\5\67\34\2\u026a\u026d\5\177@\2\u026b")
        buf.write("\u026d\5\3\2\2\u026c\u026a\3\2\2\2\u026c\u026b\3\2\2\2")
        buf.write("\u026d\u026e\3\2\2\2\u026e\u026c\3\2\2\2\u026e\u026f\3")
        buf.write("\2\2\2\u026f\u0270\3\2\2\2\u0270\u0273\5;\36\2\u0271\u0274")
        buf.write("\5\177@\2\u0272\u0274\5\3\2\2\u0273\u0271\3\2\2\2\u0273")
        buf.write("\u0272\3\2\2\2\u0274\u0275\3\2\2\2\u0275\u0273\3\2\2\2")
        buf.write("\u0275\u0276\3\2\2\2\u0276\u0277\3\2\2\2\u0277\u027a\5")
        buf.write("e\63\2\u0278\u027b\5\177@\2\u0279\u027b\5\3\2\2\u027a")
        buf.write("\u0278\3\2\2\2\u027a\u0279\3\2\2\2\u027b\u027c\3\2\2\2")
        buf.write("\u027c\u027a\3\2\2\2\u027c\u027d\3\2\2\2\u027d\u027f\3")
        buf.write("\2\2\2\u027e\u0280\13\2\2\2\u027f\u027e\3\2\2\2\u0280")
        buf.write("\u0281\3\2\2\2\u0281\u0282\3\2\2\2\u0281\u027f\3\2\2\2")
        buf.write("\u0282\u0283\3\2\2\2\u0283\u0284\5]/\2\u0284|\3\2\2\2")
        buf.write("\u0285\u0288\5!\21\2\u0286\u0289\5\177@\2\u0287\u0289")
        buf.write("\5\3\2\2\u0288\u0286\3\2\2\2\u0288\u0287\3\2\2\2\u0289")
        buf.write("\u028a\3\2\2\2\u028a\u0288\3\2\2\2\u028a\u028b\3\2\2\2")
        buf.write("\u028b\u028c\3\2\2\2\u028c\u028f\5y=\2\u028d\u0290\5\177")
        buf.write("@\2\u028e\u0290\5\3\2\2\u028f\u028d\3\2\2\2\u028f\u028e")
        buf.write("\3\2\2\2\u0290\u0291\3\2\2\2\u0291\u028f\3\2\2\2\u0291")
        buf.write("\u0292\3\2\2\2\u0292\u0293\3\2\2\2\u0293\u0296\5e\63\2")
        buf.write("\u0294\u0297\5\177@\2\u0295\u0297\5\3\2\2\u0296\u0294")
        buf.write("\3\2\2\2\u0296\u0295\3\2\2\2\u0297\u0298\3\2\2\2\u0298")
        buf.write("\u0296\3\2\2\2\u0298\u0299\3\2\2\2\u0299\u029b\3\2\2\2")
        buf.write("\u029a\u029c\13\2\2\2\u029b\u029a\3\2\2\2\u029c\u029d")
        buf.write("\3\2\2\2\u029d\u029e\3\2\2\2\u029d\u029b\3\2\2\2\u029e")
        buf.write("\u029f\3\2\2\2\u029f\u02a0\5]/\2\u02a0~\3\2\2\2\u02a1")
        buf.write("\u02a2\7\"\2\2\u02a2\u0080\3\2\2\2\33\2\u0084\u020f\u0214")
        buf.write("\u0221\u022a\u022f\u0232\u0237\u0265\u0267\u026c\u026e")
        buf.write("\u0273\u0275\u027a\u027c\u0281\u0288\u028a\u028f\u0291")
        buf.write("\u0296\u0298\u029d\3\b\2\2")
        return buf.getvalue()


class WedsqlLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    WS = 1
    CREATE = 2
    WED_FLOW = 3
    FLOW = 4
    REMOVE = 5
    BEGIN = 6
    COMMIT = 7
    ABORT = 8
    ROLLBACK = 9
    CW = 10
    LW = 11
    XD = 12
    XR = 13
    OP = 14
    WED_ATTRIBUTE = 15
    WED_CONDITION = 16
    WED_TRANSITION = 17
    WED_TRIGGER = 18
    WED_ATTRIBUTES = 19
    WED_CONDITIONS = 20
    WED_TRANSITIONS = 21
    WED_TRIGGERS = 22
    INSTANCES = 23
    INSTANCE = 24
    INCONSISTENT = 25
    TRANSACTIONING = 26
    FINAL = 27
    SET = 28
    WED_STATE = 29
    ENABLE = 30
    DISABLE = 31
    LIST = 32
    FIRED = 33
    GET = 34
    WED_TRACE = 35
    SHOW = 36
    DEFAULT = 37
    VALUES = 38
    VALUE = 39
    CONDITION = 40
    TIMEOUT = 41
    STOP = 42
    ON = 43
    TERMINATE = 44
    REINSTATE = 45
    SEMICOLON = 46
    COMMA = 47
    OPEN_PARENTESIS = 48
    CLOSE_PARENTESIS = 49
    AS = 50
    FOR = 51
    SP = 52
    LINE_COMMENT = 53
    QUOTED_VALUE = 54
    NON_SPECIAL = 55
    NAT_NUMBER = 56
    SET_FINAL_STATE_EXPR = 57
    CONDITION_EXPR = 58
    SPACE = 59

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "';'", "','", "'('", "')'", "' '" ]

    symbolicNames = [ "<INVALID>",
            "WS", "CREATE", "WED_FLOW", "FLOW", "REMOVE", "BEGIN", "COMMIT", 
            "ABORT", "ROLLBACK", "CW", "LW", "XD", "XR", "OP", "WED_ATTRIBUTE", 
            "WED_CONDITION", "WED_TRANSITION", "WED_TRIGGER", "WED_ATTRIBUTES", 
            "WED_CONDITIONS", "WED_TRANSITIONS", "WED_TRIGGERS", "INSTANCES", 
            "INSTANCE", "INCONSISTENT", "TRANSACTIONING", "FINAL", "SET", 
            "WED_STATE", "ENABLE", "DISABLE", "LIST", "FIRED", "GET", "WED_TRACE", 
            "SHOW", "DEFAULT", "VALUES", "VALUE", "CONDITION", "TIMEOUT", 
            "STOP", "ON", "TERMINATE", "REINSTATE", "SEMICOLON", "COMMA", 
            "OPEN_PARENTESIS", "CLOSE_PARENTESIS", "AS", "FOR", "SP", "LINE_COMMENT", 
            "QUOTED_VALUE", "NON_SPECIAL", "NAT_NUMBER", "SET_FINAL_STATE_EXPR", 
            "CONDITION_EXPR", "SPACE" ]

    ruleNames = [ "WS", "CREATE", "WED_FLOW", "FLOW", "REMOVE", "BEGIN", 
                  "COMMIT", "ABORT", "ROLLBACK", "CW", "LW", "XD", "XR", 
                  "OP", "WED_ATTRIBUTE", "WED_CONDITION", "WED_TRANSITION", 
                  "WED_TRIGGER", "WED_ATTRIBUTES", "WED_CONDITIONS", "WED_TRANSITIONS", 
                  "WED_TRIGGERS", "INSTANCES", "INSTANCE", "INCONSISTENT", 
                  "TRANSACTIONING", "FINAL", "SET", "WED_STATE", "ENABLE", 
                  "DISABLE", "LIST", "FIRED", "GET", "WED_TRACE", "SHOW", 
                  "DEFAULT", "VALUES", "VALUE", "CONDITION", "TIMEOUT", 
                  "STOP", "ON", "TERMINATE", "REINSTATE", "SEMICOLON", "COMMA", 
                  "OPEN_PARENTESIS", "CLOSE_PARENTESIS", "AS", "FOR", "SP", 
                  "LINE_COMMENT", "DOUBLE_QUOTES", "SINGLE_QUOTES", "QUOTED_VALUE", 
                  "NON_SPECIAL", "NAT_NUMBER", "DIGIT", "VALID_NAME", "SET_FINAL_STATE_EXPR", 
                  "CONDITION_EXPR", "SPACE" ]

    grammarFileName = "WedsqlLexer.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.7.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


