lexer grammar WedflowLexer;

ANNOTATION_WED_ATTRIBUTES : '@' [Ww] [Ee] [Dd] '-' [Aa] [Tt] [Tt] [Rr] [Ii] [Bb] [Uu] [Tt] [Ee] [Ss];

ANNOTATION_WED_CONDITIONS : '@' [Ww] [Ee] [Dd] '-' [Cc] [Oo] [Nn] [Dd] [Ii] [Tt] [Ii] [Oo] [Nn] [Ss];

ANNOTATION_WED_TRANSITIONS : '@' [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Aa] [Nn] [Ss] [Ii] [Tt] [Ii] [Oo] [Nn] [Ss];

ANNOTATION_WED_TRIGGERS : '@' [Ww] [Ee] [Dd] '-' [Tt] [Rr] [Ii] [Gg] [Gg] [Ee] [Rr] [Ss];

ANNOTATION_WED_FLOW : '@' [Ww] [Ee] [Dd] '-' [Ff] [Ll] [Oo] [Ww];

ANNOTATION_AWIC : '@' [Aa] [Ww] [Ii] [Cc];

SET_NAME : [A-Z] NON_SPECIAL*;

VALID_NAME : NON_SPECIAL+;

COLON : ':';

EQUALS : '=';

SEMICOLON : ';';

COMMA : ',';

OPEN_PARENTHESES : '(';

CLOSE_PARENTHESES : ')';

OPEN_CURLY_BRACKETS : '{';

CLOSE_CURLY_BRACKETS : '}';

LINE_COMMENT : ('//'|'--') .*? '\r'? '\n' -> skip;

fragment NON_SPECIAL : [a-zA-Z] | DIGIT | [\-_];

fragment DIGIT : [0-9];

SPACE : ' ';

LINEBREAK : '\r'? '\n';

WS : [\t]+->skip;

DEFINITION : VALID_NAME (SPACE | LINEBREAK)* COLON .+? SEMICOLON;
