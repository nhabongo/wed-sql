parser grammar Wedflow;
options { tokenVocab=WedflowLexer; }

wflow: stmt_list;

stmt_list: empty_space stmt empty_space stmt_list 
         | empty_space stmt empty_space;

stmt: create_flow_stmt empty_space SEMICOLON
    | create_attr_stmt empty_space SEMICOLON 
    | create_cond_stmt empty_space
    | create_trans_stmt empty_space 
    | create_trig_stmt empty_space  
    | set_fstate_stmt empty_space SEMICOLON;

create_flow_stmt: ANNOTATION_WED_FLOW SPACE* LINEBREAK empty_space valid_name empty_space EQUALS empty_space wed_flow_set_expr;

create_attr_stmt: ANNOTATION_WED_ATTRIBUTES SPACE* LINEBREAK empty_space SET_NAME empty_space EQUALS empty_space set_expr;

create_cond_stmt: ANNOTATION_WED_CONDITIONS SPACE* LINEBREAK empty_space SET_NAME empty_space EQUALS empty_space set_expr empty_space SEMICOLON empty_space (condition_def empty_space)*;

create_trans_stmt: ANNOTATION_WED_TRANSITIONS SPACE* LINEBREAK empty_space SET_NAME empty_space EQUALS empty_space set_expr empty_space SEMICOLON empty_space (transition_def empty_space)*;

create_trig_stmt: ANNOTATION_WED_TRIGGERS SPACE* LINEBREAK empty_space SET_NAME empty_space EQUALS empty_space set_expr empty_space SEMICOLON empty_space (trigger_def empty_space)*;

set_fstate_stmt: ANNOTATION_AWIC SPACE* LINEBREAK empty_space SET_NAME empty_space EQUALS empty_space set_expr;


set_expr : OPEN_CURLY_BRACKETS empty_space ((valid_name empty_space COMMA empty_space)* valid_name empty_space)? CLOSE_CURLY_BRACKETS;

wed_flow_set_expr : OPEN_CURLY_BRACKETS empty_space SET_NAME empty_space COMMA empty_space valid_name empty_space COMMA empty_space  valid_name empty_space CLOSE_CURLY_BRACKETS;

condition_def : DEFINITION;

transition_def : DEFINITION;

trigger_def : DEFINITION;

valid_name : (SET_NAME | VALID_NAME);

empty_space : (SPACE | LINEBREAK)*;
