#!/bin/bash

alias antlr4='java -jar /usr/local/lib/antlr-4.7.1-complete.jar'

antlr4 -Dlanguage=Python3 Wedflow.g4 WedflowLexer.g4 
mv -f WedflowLexer.py ../../compilers/wedflow_compiler/WedflowLexer.py
mv -f Wedflow.py ../../compilers/wedflow_compiler/Wedflow.py
mv -f WedflowListener.py ../../compilers/wedflow_compiler/WedflowListener.py
