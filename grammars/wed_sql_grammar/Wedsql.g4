parser grammar Wedsql;
options { tokenVocab=WedsqlLexer; }

wsql: stmt_list;

stmt_list: SPACE* stmt SPACE* stmt_list 
         | SPACE* stmt SPACE*;

stmt: create_flow_stmt SPACE* SEMICOLON
    | remove_flow_stmt SPACE* SEMICOLON
    | create_attr_stmt SPACE* SEMICOLON 
    | create_cond_stmt 
    | create_trans_stmt SPACE* SEMICOLON
    | create_trig_stmt SPACE* SEMICOLON
    | create_inst_stmt SPACE* SEMICOLON
    | set_fstate_stmt 
    | enable_trig_stmt SPACE* SEMICOLON
    | disable_trig_stmt SPACE* SEMICOLON
    | list_attr_stmt SPACE* SEMICOLON
    | list_cond_stmt SPACE* SEMICOLON 
    | list_trans_stmt SPACE* SEMICOLON 
    | list_trig_stmt SPACE* SEMICOLON
    | list_ftrans_stmt SPACE* SEMICOLON
    | list_insts_stmt SPACE* SEMICOLON
    | show_fstate_stmt SPACE* SEMICOLON
    | get_trace_stmt SPACE* SEMICOLON
    | terminate_stmt SPACE* SEMICOLON
    | reinstate_stmt SPACE* SEMICOLON
    | begin_stmt SPACE* SEMICOLON
    | commit_stmt SPACE* SEMICOLON
	| abort_stmt SPACE* SEMICOLON
	| shell_connect_flow_stmt SPACE* SEMICOLON
	| shell_list_flow_stmt SPACE* SEMICOLON
	| shell_debug_msg_stmt SPACE* SEMICOLON
	| shell_raw_output_stmt SPACE* SEMICOLON
	| shell_open_file_stmt SPACE* SEMICOLON
	| LINE_COMMENT;

create_flow_stmt: CREATE SPACE+ FLOW SPACE+ valid_name;

remove_flow_stmt: REMOVE SPACE+ FLOW SPACE+ valid_name;

create_attr_stmt: CREATE SPACE+ WED_ATTRIBUTE SPACE+ valid_name SPACE* opt_def_value_expr;

create_cond_stmt: CREATE SPACE+ CONDITION_EXPR;

create_trans_stmt: CREATE SPACE+ WED_TRANSITION SPACE+ valid_name SPACE* opt_stop_expr SPACE* opt_proc_expr;

create_trig_stmt: CREATE SPACE+ WED_TRIGGER SPACE+ valid_name SPACE+ AS SPACE+ trigger_expr;

create_inst_stmt: CREATE SPACE+ INSTANCE SPACE+ AS SPACE+ inst_expr;

set_fstate_stmt: SET SPACE+ SET_FINAL_STATE_EXPR;

enable_trig_stmt: ENABLE SPACE+ WED_TRIGGER SPACE+ valid_name;

disable_trig_stmt: DISABLE SPACE+ WED_TRIGGER SPACE+ valid_name;

list_attr_stmt: LIST SPACE+ WED_ATTRIBUTES | LIST SPACE+ WED_ATTRIBUTES SPACE+ valid_name;

list_cond_stmt: LIST SPACE+ WED_CONDITIONS | LIST SPACE+ WED_CONDITIONS SPACE+ valid_name;

list_trans_stmt: LIST SPACE+ WED_TRANSITIONS | LIST SPACE+ WED_TRANSITIONS SPACE+ valid_name;

list_trig_stmt: LIST SPACE+ WED_TRIGGERS | LIST SPACE+ WED_TRIGGERS SPACE+ valid_name;

list_ftrans_stmt: LIST SPACE+ FIRED SPACE+ WED_TRANSITIONS SPACE* opt_id;

list_insts_stmt: LIST SPACE+ INSTANCES | LIST SPACE+ TRANSACTIONING SPACE+ INSTANCES | LIST SPACE+ INCONSISTENT SPACE+ INSTANCES | LIST SPACE+ FINAL SPACE+ INSTANCES | LIST SPACE+ INSTANCES SPACE* opt_id | LIST SPACE+ TRANSACTIONING SPACE+ INSTANCES SPACE* opt_id | LIST SPACE+ INCONSISTENT SPACE+ INSTANCES SPACE* opt_id | LIST SPACE+ FINAL SPACE+ INSTANCES SPACE* opt_id;

show_fstate_stmt: SHOW SPACE+ FINAL SPACE+ WED_STATE;

get_trace_stmt: GET SPACE+ WED_TRACE SPACE+ FOR SPACE+ NAT_NUMBER;

terminate_stmt: TERMINATE SPACE+ INSTANCE SPACE+ NAT_NUMBER;

reinstate_stmt: REINSTATE SPACE+ INSTANCE SPACE+ NAT_NUMBER;
    
begin_stmt: BEGIN;

commit_stmt: COMMIT;

abort_stmt: ABORT | ROLLBACK;

shell_connect_flow_stmt: CW SPACE+ valid_name;

shell_list_flow_stmt: LW;

shell_debug_msg_stmt: XD;

shell_raw_output_stmt: XR;

shell_open_file_stmt: OP SPACE+ QUOTED_VALUE;

opt_def_value_expr: /*nil*/
                  | DEFAULT SPACE+ VALUE SPACE+ value_expr;

value_expr: QUOTED_VALUE
          | NAT_NUMBER;

opt_stop_expr: /*nil*/
             | STOP SPACE+ ON SPACE+ (stop_condition_expr (SPACE+ timeout_expr)? SPACE* | timeout_expr (SPACE+ stop_condition_expr)? SPACE*);

timeout_expr: TIMEOUT SPACE+ QUOTED_VALUE;

stop_condition_expr: CONDITION SPACE+ valid_name;

opt_proc_expr: /*nil*/
             | AS SPACE+ SP;

trigger_expr : OPEN_PARENTESIS SPACE* valid_name SPACE* COMMA SPACE* valid_name SPACE* CLOSE_PARENTESIS;

inst_expr : DEFAULT SPACE+ VALUES 
	| attribute_list_expr value_list_expr;

value_list_expr : SPACE+ VALUES SPACE+ OPEN_PARENTESIS SPACE* value_expr SPACE* (COMMA SPACE* value_expr SPACE*)* CLOSE_PARENTESIS ;

attribute_list_expr : OPEN_PARENTESIS SPACE* valid_name SPACE* (COMMA SPACE* valid_name SPACE*)* CLOSE_PARENTESIS;

opt_id : /*nil*/ 
        | FOR SPACE* NAT_NUMBER;

valid_name : (NON_SPECIAL | NAT_NUMBER | reserved_words)+;

reserved_words : (CREATE | WED_FLOW | FLOW | REMOVE | BEGIN | COMMIT | ABORT | ROLLBACK | WED_ATTRIBUTE | WED_CONDITION | WED_TRANSITION | WED_TRIGGER | WED_ATTRIBUTES | WED_CONDITIONS | WED_TRANSITIONS | WED_TRIGGERS | INSTANCES | INSTANCE | INCONSISTENT | TRANSACTIONING | FINAL | SET | WED_STATE | ENABLE | DISABLE | LIST | FIRED | GET | WED_TRACE | SHOW | DEFAULT | VALUES | VALUE | CONDITION | TIMEOUT | STOP | ON | AS | FOR | SP | TERMINATE | REINSTATE);



