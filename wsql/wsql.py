#!/usr/bin/env python3

import sys
import re
import os
import atexit
import subprocess
import readline
from importlib import util as imputil

#check if the packages bellow are installed
antlr4 = imputil.find_spec('antlr4')

if not antlr4:
    print('python package \'antlr4\' is not installed !')
    sys.exit(1)


from antlr4 import *
from WSP import WedActions
from WSP import SQLModeListener
from WSP import WedsqlErrorStrategy
from WSP import WedsqlErrorListener
sys.path.insert(0, sys.path[0]+'/../compilers/wedsql_compiler')
from Wedsql import Wedsql 
from WedsqlLexer import WedsqlLexer 
from WedsqlListener import WedsqlListener
from antlr4.error.ErrorStrategy import ErrorStrategy
from antlr4.error.ErrorListener import ErrorListener
from antlr4.error.Errors import (RecognitionException, NoViableAltException, 
InputMismatchException, FailedPredicateException, ParseCancellationException)

dirname = os.path.dirname(sys.argv[0])
if dirname:
    os.chdir(dirname)

source_files_path = "Install/"
history_file = ".pyser_hist"

#TODO: check for plpython3
#TODO: Detect if postgresql is up and running (in case it is, find the config file :
#      ps xao pid,ppid,comm,args | awk -F' ' '($2 == 1) && ($3 == "postgres") {print $6}')
def find_postgresql():

    print("Looking for PostgreSQL: ", end='')
    ps = subprocess.Popen(['ps', 'wwfxao', 'pid,ppid,comm,args'], stdout=subprocess.PIPE)
    awk = subprocess.Popen(['awk', '-F ', '($2 == 1) && ($3 == "postgres") {print $0}'], stdin=ps.stdout, stdout=subprocess.PIPE, universal_newlines=True)
    ps.stdout.close()
    pg_base = awk.communicate()[0]
    
    if pg_base:
        print("\033[0;32mFound\033[0m.")
        
        pg_cfg_ubuntu = re.search(r"config_file=(.+\.conf)", pg_base)
        pg_cfg_arch = re.search(r'-D\s*(\S+)', pg_base)
        
        if pg_cfg_ubuntu:
            print('Config file: \033[2;37m%s\033[0m' %(pg_cfg_ubuntu.group(1)))
            return pg_cfg_ubuntu.group(1).strip()
        
        elif pg_cfg_arch:
            cfg_path = pg_cfg_arch.group(1).strip() + "/postgresql.conf"
            print('Config file: \033[2;37m%s\033[0m' %(cfg_path))
            return cfg_path
       
        else:
            raise OSError("Postgresql config file not found")
          
    else:
        raise ProcessLookupError("postgres process not found")


def main(argv):
    
    if sys.hexversion < 0x3000000:
        sys.exit("Python version >= 3.0 is required !")
    
    #check if postgresql is up and running
    try:
        pg_config_file = find_postgresql()
    except Exception as e:
        print(e)
        exit(1)
    
    #configure the readline library
    readline.parse_and_bind('tab: complete')
    readline.parse_and_bind('set editing-mode vi')
    
    try:
        readline.read_history_file(history_file)
    except FileNotFoundError:
        print("Commands history file not found.")
    else:
        readline.set_history_length(1000)
    
    atexit.register(readline.write_history_file, history_file)
    
    #start the main loop
    print("Wed-flow Management Tool v0.1 (python version: %s.%s.%s)" % sys.version_info[:3])
    a = WedActions(pg_config_file, source_files_path)
    try:
        carry = ""
        while True:
            if (a.reading_file):
                try:
                    with open(a.input_file_path, 'r') as input_file:
                        new_input = carry + '\n' + input_file.read();
                    input_file.close()
                    a.reading_file = False
                    a.input_file_path = None
                except IOError:
                    print ("Error reading input file: " + a.input_file_path)
            else:
                p_symbol = a.database+'=> '
                new_input = carry + '\n' + input(p_symbol)
            carry = new_input.split(';')[-1]
            complete_lines = new_input.split(';')[:-1]
            for line in complete_lines:
                line = line + ';'
                try:
                    input_stream = InputStream(line)
                    lexer = WedsqlLexer(input_stream)
                    stream = CommonTokenStream(lexer)
                    parser = Wedsql(stream)
                    lexer.removeErrorListeners()
                    parser._errHandler = WedsqlErrorStrategy()
                    tree = parser.wsql()
                    walker = ParseTreeWalker()
                    walker.walk(a, tree)
                except Exception:
                    try:
                        a.SQL(line)
                    except Exception as e:
                        print(e)
    except KeyboardInterrupt:
        print()
        return 1
    except EOFError:
        print('\q')
        
    return 0
    
if __name__ == '__main__':
    sys.exit(main(sys.argv))
