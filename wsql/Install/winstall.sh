#!/bin/bash

TEMPLATE='saga_timeout_template'
DB=$1
WORKER=${DB}_saga_timeout
USER=$2
ADMIN_USER=admin_${USER}
CONFIG=$3

if [[ $# < 3 ]]
then
	echo "$0 <database name> <wedflow user> <postgresql.conf file>" >&2
	exit 1
elif [[ $UID != 0 ]]
then
	echo "Need to be root!" >&2
	exit 1
elif [[ ! -f $3 ]]
then
	echo "File $3 not found" >&2
	exit 1
fi
echo -e "Generating new WED-flow"
echo -n "New password for user $USER :" 
read -s PASSX
echo -ne "\nNew password for user $USER :" 
read -s PASSY
echo ''

if [[ $PASSX != $PASSY ]]
then
    echo "Passwords don't match, aborting ..." >&2
    exit 1 
fi

echo -e "Creating new WED-flow database ..."

psql -U postgres -q -c "CREATE DATABASE $DB ;"
if [[ $? != 0 ]]
then
    exit 1
fi 

psql -U postgres -q -c "CREATE ROLE $ADMIN_USER WITH LOGIN PASSWORD '$PASSX' SUPERUSER;"
psql -U postgres -q -c "CREATE ROLE $USER WITH LOGIN PASSWORD '$PASSX' ;"
if [[ $? != 0 ]]
then
    psql -U postgres -q -c "DROP DATABASE $DB ;"
    exit 1
else
    psql -U postgres -q -c "REVOKE ALL ON DATABASE $DB FROM public;"
    psql -U postgres -q -c "REVOKE ALL ON SCHEMA public FROM public;"
    psql -U postgres -q -c "GRANT CONNECT ON DATABASE $DB TO $USER ;"
fi 

echo -e "Installing WED-flow on database $DB ..."

psql -U postgres -v ON_ERROR_STOP=1 -q -d $DB -f WED-flow.sql 

if [[ $? != 0 ]]
then
    psql -U postgres -q -c "DROP DATABASE $DB ;"
    psql -U postgres -q -c "DROP ROLE $USER ;"
    psql -U postgres -q -c "DROP ROLE $ADMIN_USER ;"
    exit 1
fi

echo -e "Setting WED-user permissions on database $DB ..."

echo -e "GRANT USAGE ON SCHEMA public TO $USER;
         GRANT SELECT ON job_pool to $USER;
         GRANT SELECT ON wed_trace to $USER;
         GRANT SELECT,INSERT,UPDATE ON wed_attr to $USER;
         GRANT SELECT,INSERT,UPDATE ON wed_cond to $USER;
         GRANT SELECT,INSERT,UPDATE ON wed_trans to $USER;
         GRANT SELECT,UPDATE ON wed_flow to $USER;
         GRANT SELECT,INSERT ON wed_trig to $USER;
         REVOKE ALL ON ALL FUNCTIONS IN SCHEMA public FROM public;
         REVOKE ALL ON ALL FUNCTIONS IN SCHEMA public FROM $USER;
         GRANT USAGE ON ALL SEQUENCES IN SCHEMA public to $USER;" > tmp_priv
psql -U postgres -q -d $DB -f tmp_priv
rm tmp_priv

echo -e  "Generating new bg_worker ..."
rm -rf $WORKER > /dev/null 2>&1
cp -r $TEMPLATE $WORKER
cd $WORKER

mv saga_timeout--1.0.sql ${WORKER}--1.0.sql
mv saga_timeout.c ${WORKER}.c
mv saga_timeout.control ${WORKER}.control

#rename wed_worker $WORKER *
sed -i "s/saga_timeout/$WORKER/g" *
sed -i "s/__DB_NAME__/\"$DB\"/" $WORKER.c
make > /dev/null

echo -e "Installing bg_worker ...\n"
make install
cd ../
echo ""
python3 pg_worker_register.py $WORKER $CONFIG 
if [[ $? != 0 ]]
then
	psql -U postgres -q -d $DB -c "DROP OWNED BY $USER ;"
	psql -U postgres -q -d $DB -c "DROP DATABASE $DB ;"
	psql -U postgres -q -d $DB -c "DROP ROLE $USER ;"
	psql -U postgres -q -d $DB -c "DROP ROLE $ADMIN_USER ;"
else
	echo -e "Restarting postgresql server ..."
	service postgresql restart
    if [[ $? != 0 ]]
    then
        /etc/init.d/postgresql restart
    fi
	echo -e "Cleaning temporary files ..."
	rm -rf $WORKER
fi

echo "DONE !"
exit 0
