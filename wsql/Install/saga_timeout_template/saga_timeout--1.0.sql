/* saga_tiemout/saga_timeout--1.0.sql */

-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION saga_timeout" to load this file. \quit

-- Dynamic background laucher
--CREATE OR REPLACE FUNCTION saga_timeout_launch(pg_catalog.int4, pg_catalog.text)
--RETURNS pg_catalog.int4 STRICT
--AS 'MODULE_PATHNAME'
--LANGUAGE C;
