import pathlib
import subprocess
import psycopg2
import sys
from antlr4 import *
sys.path.insert(0, sys.path[0]+'/../compilers/wedsql_compiler')
from Wedsql import Wedsql 
from WedsqlLexer import WedsqlLexer 
from WedsqlListener import WedsqlListener 
from WedsqlTranslator import WedsqlTranslator

class WedActions(WedsqlListener):

    def __init__(self, pg_config_file, source_files_path, raw_output=False, dbg_msg=False):
        #TODO: raise error if the install folder is missing
        self.source_files_path = pathlib.Path(source_files_path).resolve()
        self.pg_config_file = pg_config_file
        self.pg_connection = None
        self.database = ""
        self.raw_output = raw_output
        self.dbg_msg = dbg_msg
        self.active_transaction = False
        self.translator = WedsqlTranslator()
        self.reading_file = False
        self.input_file_path = None
        

    def enterBegin_stmt(self, ctx):
        if not self.pg_connection:
            raise Exception("ERROR: Not connected to any wed-flow database !")
        if not self.active_transaction: 
            self.active_transaction = True
        else:
            print("A transaction is already active")
    
    def enterCommit_stmt(self, ctx):
        if not self.pg_connection:
            raise Exception("ERROR: Not connected to any wed-flow database !")
        if self.active_transaction:
            self.pg_connection.commit()
            self.active_transaction = False
        else:
            print("Not in an open transaction")
                
    def enterAbort_stmt(self, ctx):
        if not self.pg_connection:
            raise Exception("ERROR: Not connected to any wed-flow database !")
        if self.active_transaction:
            self.pg_connection.rollback()
            self.active_transaction = False
        else:
            print("Not in an open transaction")

    #TODO: rewrite winstall.sh in python
    def enterCreate_flow_stmt(self, ctx):
        wed_flow_name = ctx.valid_name().getText()
        a = subprocess.Popen(["sudo","./winstall.sh", wed_flow_name, wed_flow_name, self.pg_config_file], cwd="Install", universal_newlines=True)
        a.communicate()[0] 

    #TODO: confirm password before removing wed-flow
    def enterRemove_flow_stmt(self, ctx):
        wed_flow_name = ctx.valid_name().getText()
        a = subprocess.Popen(["sudo","./wremove.sh", wed_flow_name, wed_flow_name, self.pg_config_file], cwd="Install", universal_newlines=True)
        a.communicate()[0] 

    def enterCreate_attr_stmt(self, ctx):
        (stmt, params) = self.translator.create_attribute(ctx)
        self.SQL(stmt, params)

    def enterCreate_cond_stmt(self, ctx):
        (stmt, params) = self.translator.create_condition(ctx)
        self.SQL(stmt,params)

    def enterCreate_trans_stmt(self, ctx):
        (stmt, params) = self.translator.create_transition(ctx)
        self.SQL(stmt, params)

    def enterCreate_trig_stmt(self, ctx):
        (stmt, params) = self.translator.create_trigger(ctx)
        self.SQL(stmt,params)

    #TODO: prevent user from choosing wid
    def enterCreate_inst_stmt(self, ctx):
        (stmt, params) = self.translator.create_instance(ctx)
        self.SQL(stmt, params)

    def enterSet_fstate_stmt(self, ctx):
        (stmt, params) = self.translator.set_final_state(ctx)
        self.SQL(stmt, params)

    def enterEnable_trig_stmt(self, ctx):
        (stmt, params) = self.translator.enable_trigger(ctx)
        self.SQL(stmt, params)

    def enterDisable_trig_stmt(self, ctx):
        (stmt, params) = self.translator.disable_trigger(ctx)
        self.SQL(stmt,params)

    def enterList_attr_stmt(self, ctx):
        (stmt, params) = self.translator.list_attributes(ctx)
        self.SQL(stmt,params)

    def enterList_cond_stmt(self, ctx):
        (stmt, params) = self.translator.list_conditions(ctx)
        self.SQL(stmt,params)

    def enterList_trans_stmt(self, ctx):
        (stmt, params) = self.translator.list_transitions(ctx)
        self.SQL(stmt,params)

    def enterList_trig_stmt(self, ctx):
        (stmt, params) = self.translator.list_triggers(ctx)
        self.SQL(stmt,params)

    def enterList_ftrans_stmt(self, ctx):
        (stmt, params) = self.translator.list_fired_transitions(ctx, self.database)
        self.SQL(stmt,params)

    def enterList_insts_stmt(self, ctx):
        (stmt, params) = self.translator.list_instances(ctx)
        self.SQL(stmt,params)

    def enterShow_fstate_stmt(self, ctx):
        (stmt, params) = self.translator.show_final_state(ctx)
        self.SQL(stmt,params)

    def enterGet_trace_stmt(self, ctx):
        (stmt, params) = self.translator.get_trace(ctx)
        self.SQL(stmt,params)
        
    def enterTerminate_stmt(self, ctx):
        (stmt, params) = self.translator.terminate_instance(ctx)
        self.SQL(stmt,params)
    
    def enterReinstate_stmt(self, ctx):
        (stmt, params) = self.translator.reinstate_instance(ctx)
        self.SQL(stmt,params)
        
    #TODO: create a shell user in the db to not use the postgres user
    def enterShell_list_flow_stmt(self, ctx):
        
        query = 'select setting as "WED-flow" from pg_settings where short_desc = \'WED-flow database\''
        
        if self.pg_connection:
            self.pSQL(query)
        else:
            try:
                a = subprocess.Popen(["psql", "-U", "postgres", "-d", "template1", "-P", "footer=off", "-c", query], universal_newlines=True)
                a.communicate()[0]
            except Exception as e:
                print("Connection Error: could not connect to postgresql")
    
    #TODO: check before connect if the database is a wed-flow database
    def enterShell_connect_flow_stmt(self, ctx):
        database = ctx.valid_name().getText()
        admin_user = "admin_"+database 
        try:
            conn = psycopg2.connect(database=database, user=admin_user, password="")
        except Exception as e:
            print("Connection Error: could not connect to wed-flow \"%s\"" %(database))
        else:
            #maybe I should check for a previous connection and close it before starting a new one =P
            self.pg_connection = conn
            self.database = database
    
    def enterShell_debug_msg_stmt(self, ctx):
        self.dbg_msg = not self.dbg_msg
    
    def enterShell_raw_output_stmt(self, ctx):
        self.raw_output = not self.raw_output
        
    def enterShell_open_file_stmt(self, ctx):
        self.reading_file = True
        self.input_file_path = ctx.QUOTED_VALUE().getText()
        self.input_file_path = self.input_file_path[1:-1]
       
    #TODO: execute psql meta commands
    def SQL(self, stmt, params = None):
        if not stmt:
            return
        if stmt[0] == '\\':
            self.pSQL(stmt, params)
            
        elif self.pg_connection:
            cur = self.pg_connection.cursor()
            if params:
                stmt = cur.mogrify(stmt, params)
            #DEBUG MSG
            self.dbg_msg and print("SQL: %s" % (stmt))
            try:
                cur.execute(stmt)
            except Exception as e:
                print(e)
            else:
                if cur.description:
                    try:
                        res = cur.fetchall()
                    except Exception as e:
                        print(e)
                    else:    
                        self.raw_print(cur.description, res, cur.rowcount) or self.resultPrint(cur.description, res, cur.rowcount)
                else:
                    print(cur.statusmessage)        
            cur.close()
            if not self.active_transaction:
                self.pg_connection.commit()
        else:
            raise Exception("ERROR: Not connected to any wed-flow database !")
    
    def pSQL(self, stmt, params = None):
        if self.pg_connection:
            if params:
                cur = self.pg_connection.cursor()
                stmt = cur.mogrify(stmt, params)
                
            a = subprocess.Popen(["psql", "-U", self.database, "-P", "footer=off", "-c", stmt], universal_newlines=True)
            a.communicate()[0]
        else:
            raise Exception("ERROR: Not connected to any wed-flow database !")
    
    
    def raw_print(self, header, rows, count):
        if self.raw_output:
            print(rows)
            return True
        return False
    
    @staticmethod     
    def resultPrint(header, rows, count):
        
        nmax_len = [len(x.name) for x in header]
        
        if rows:
            vmax_len = [len(str(max(rows, key=lambda x: len(str(x[i])))[i])) for i in range(len(header))]
            max_len = [ nmax_len[i] if nmax_len[i] > vmax_len[i] else vmax_len[i] for i in range(len(nmax_len))]
        else:
            max_len = nmax_len
        #for a in header:
        #    print((a.name,a.type_code))
        
        tpl_str = ' | '.join('{: ^'+str(i)+'s}' for i in max_len)
        tpl_str = ' ' + tpl_str
        
        spr_str = '+'.join('-' * i + '--' for i in max_len)
        
        print()
        print(tpl_str.format(*[c.name for c in header]))
        print(spr_str)
        for r in rows:
            print(tpl_str.format(*[str(v) if v is not None else '' for v in r]))
        print('(%d row%s)\n' % (count, 's' if count != 1 else ''))
            
        








            
