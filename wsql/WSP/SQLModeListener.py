import pathlib
import psycopg2
import sys
from WSP import WedActions
from antlr4 import *
sys.path.insert(0, sys.path[0]+'/../compilers/wedsql_compiler')
from Wedsql import Wedsql 
from WedsqlLexer import WedsqlLexer 
from WedsqlListener import WedsqlListener 
from WedsqlTranslator import WedsqlTranslator

class SQLModeListener(WedsqlListener):

    def __init__(self, wed_actions):
        self.wed_actions = wed_actions
        self.translator = WedsqlTranslator()

    def enterBegin_stmt(self, ctx):
        if not self.wed_actions.pg_connection:
            raise Exception("ERROR: Not connected to any wed-flow database !")
        if not self.active_transaction: 
            self.wed_actions.active_transaction = True
        else:
            print("A transaction is already active")
    
    def enterCommit_stmt(self, ctx):
        if not self.wed_actions.pg_connection:
            raise Exception("ERROR: Not connected to any wed-flow database !")
        if self.wed_actions.active_transaction:
            self.wed_actions.pg_connection.commit()
            self.wed_actions.active_transaction = False
        else:
            print("Not in an open transaction")
                
    def enterAbort_stmt(self, ctx):
        if not self.wed_actions.pg_connection:
            raise Exception("ERROR: Not connected to any wed-flow database !")
        if self.wed_actions.active_transaction:
            self.wed_actions.pg_connection.rollback()
            self.wed_actions.active_transaction = False
        else:
            print("Not in an open transaction")

    def enterCreate_flow_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterRemove_flow_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterCreate_attr_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterCreate_cond_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterCreate_trans_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterCreate_trig_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterCreate_inst_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterSet_fstate_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterEnable_trig_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterDisable_trig_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterList_attr_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterList_cond_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterList_trans_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterList_trig_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterList_ftrans_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterList_insts_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterShow_fstate_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")

    def enterGet_trace_stmt(self, ctx):
        raise Exception("ERROR: Command not avaliable in SQL mode !")
    
    #TODO: create a shell user in the db to not use the postgres user
    def enterShell_list_flow_stmt(self, ctx):
        
        query = 'select setting as "WED-flow" from pg_settings where short_desc = \'WED-flow database\''
        
        if self.wed_actions.pg_connection:
            self.wed_actions.pSQL(query)
        else:
            try:
                a = subprocess.Popen(["psql", "-U", "postgres", "-d", "template1", "-P", "footer=off", "-c", query], universal_newlines=True)
                a.communicate()[0]
            except Exception as e:
                print("Connection Error: could not connect to postgresql")
    
    #TODO: check before connect if the database is a wed-flow database
    def enterShell_connect_flow_stmt(self, ctx):
        database = ctx.valid_name().getText()
        try:
            conn = psycopg2.connect(database=database, user=database, password="")
        except Exception as e:
            print("Connection Error: could not connect to wed-flow \"%s\"" %(database))
        else:
            #maybe I should check for a previous connection and close it before starting a new one =P
            self.wed_actions.pg_connection = conn
            self.wed_actions.database = database
    
    def enterShell_debug_msg_stmt(self, ctx):
        self.wed_actions.dbg_msg = not self.wed_actions.dbg_msg
    
    def enterShell_raw_output_stmt(self, ctx):
        self.wed_actions.raw_output = not self.wed_actions.raw_output
        
    def enterShell_sql_mode_stmt(self, ctx):
        self.wed_actions.sql_mode = not self.wed_actions.sql_mode
       
