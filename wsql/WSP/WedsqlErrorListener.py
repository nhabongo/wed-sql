from antlr4 import * 
from antlr4.error.ErrorListener import ErrorListener
from WSP import WedActions

class WedsqlErrorListener(ErrorListener):

    def __init__(self, wed_actions:WedActions):
        super().__init__()
        self.wed_actions = wed_actions 

    def syntaxError(self, recognizer:Lexer, offendingSymbol, line, column, msg, e):
        self.wed_actions.SQL(str(recognizer._input))
        raise e
