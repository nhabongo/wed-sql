from antlr4 import * 
from antlr4.error.ErrorStrategy import BailErrorStrategy
from antlr4.error.Errors import RecognitionException, NoViableAltException, InputMismatchException, FailedPredicateException, ParseCancellationException
from WSP import WedActions

class WedsqlErrorStrategy(BailErrorStrategy):

    def __init__(self):
        super().__init__()

    def reportNoViableAlternative(self, recognizer:Parser, e:NoViableAltException):
        pass

    def reportInputMismatch(self, recognizer:Parser, e:InputMismatchException):
        pass

    def reportUnwantedToken(self, recognizer:Parser):
        pass

    def reportFailedPredicate(self, recognizer:Parser):
        pass
