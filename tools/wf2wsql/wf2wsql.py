# encoding: utf-8
import sys
import os
from antlr4 import *
sys.path.insert(0, sys.path[0]+'/../../compilers/wedflow_compiler')
from Wedflow import Wedflow 
from WedflowLexer import WedflowLexer 
from WedflowListener import WedflowListener 
from WedflowTranslator import WedflowTranslator 
from WedflowSemantic import WedflowSemantic 

class Wedflow2Wedsql(WedflowListener):

    cond_def = {}
    awic = []
    textCreateFlow = ""
    textCreateAttributes = ""
    textCreateConditions = ""
    textCreateTransitions = ""
    textCreateTriggers = ""
    textSetFinalState = ""
        
    def __init__(self, input_file_name):
        output_base = os.path.basename(input_file_name)
        self.output_file_name = os.path.splitext(output_base)[0] + ".wsql"
        self.output = open(self.output_file_name, 'w')
        self.translator = WedflowTranslator()
        self.semantic = WedflowSemantic()
        self.errors = ""

    def enterCreate_flow_stmt(self, ctx):
        self.semantic.read_create_flow(ctx)
        self.textCreateFlow = self.translator.create_flow(ctx)

    def enterCreate_attr_stmt(self, ctx):
        self.textCreateAttributes += self.translator.create_attributes(ctx)

    def enterCreate_cond_stmt(self, ctx):
        self.errors += self.semantic.check_condition_set(ctx)
        self.textCreateConditions = self.translator.create_conditions(ctx)

    def enterCreate_trans_stmt(self, ctx):
        self.errors += self.semantic.check_transition_set(ctx)
        self.textCreateTransitions = self.translator.create_transitions(ctx)

    def enterCreate_trig_stmt(self, ctx):
        self.errors += self.semantic.check_trigger_set(ctx)
        self.textCreateTriggers = self.translator.create_triggers(ctx)

    def enterSet_fstate_stmt(self, ctx):
        final_condition_list = ctx.set_expr().valid_name()
        for final_condition in final_condition_list:
            self.awic.append(final_condition.getText())

    def exitWflow(self, ctx):
        self.errors += self.semantic.check_awic(self.awic)
        self.errors += self.semantic.check_flow()
        self.errors += self.semantic.check_triggers()
        if self.errors == "":
            self.output.write(self.textCreateFlow)
            self.output.write(self.textCreateAttributes)
            self.output.write(self.textCreateConditions)
            self.output.write(self.textCreateTransitions)
            self.output.write(self.textCreateTriggers)
            self.textSetFinalState = self.translator.set_final_state(self.awic)
            self.output.write(self.textSetFinalState)
            self.output.write("commit;\n")
        else:
            print(self.errors)
        self.output.close()

def main(argv):
    input = FileStream(argv[1], encoding="utf-8")
    lexer = WedflowLexer(input)
    stream = CommonTokenStream(lexer)
    parser = Wedflow(stream)
    tree = parser.wflow()
    printer = Wedflow2Wedsql(argv[1])
    walker = ParseTreeWalker()
    walker.walk(printer, tree)

if __name__ == '__main__':
    main(sys.argv)
