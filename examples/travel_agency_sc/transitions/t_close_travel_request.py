from WEDWorker import WEDWorkerTemplate
import sys,random
import time

class t_close_travel_request(WEDWorkerTemplate):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 't_close_travel_request'
    dbs = 'user=travel_agency_sc dbname=travel_agency_sc application_name=t_close_travel_request'
    wakeup_interval = 5
    
    def __init__(self):
        super().__init__(self.trname, self.dbs, self.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self, wed_state, wid):
        print("\033[33m> Finalizing order (Id = %d)\033[0m" % wid)
        print ("\033[33m> WED-state: %s\033[0m" % wed_state)
        
        #business logic goes here 
        time.sleep(1) 
        return "order_status = 'Finalized'"
        
w = t_close_travel_request()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

