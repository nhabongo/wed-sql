from BaseWorker import BaseClass
import sys,psycopg2,json,random

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'coletar_materiais'
    dbs = 'user=coleta dbname=coleta application_name=ww-tr_mat'
    wakeup_interval = 5
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        print ('\nFetching projects from entrevista ...\n')
        
        materiais_coletados = []
        materiais_eliminados = []
        
        projetos_acordados = json.loads(self.get_proj_acord(payload['bid']))
        
        for p in projetos_acordados:
            if random.random() < 0.5:
                materiais_coletados.append(p['material'])
                print("[\033[32mCOLETADO\033[0m] Material: %s" % (p['material']))
            else:
                materiais_eliminados.append(p['material'])
                print("[\033[31mNAO COLETADO\033[0m] Material: %s" % (p['material']))
        
        return "materiais_coletados='%s', materiais_eliminados='%s', mem='%s'" % (json.dumps(materiais_coletados), json.dumps(materiais_eliminados), 'material estragado' if materiais_eliminados else '')
        #return None
    
    def get_proj_acord(self, bid):
        
        try:
            conn = psycopg2.connect("dbname=entrevista user=entrevista")
        except Exception as e:
            print(e)
            exit(-1)
            
        cur = conn.cursor()
        try:
            cur.execute('select projetos_acordados from wed_flow where bid = %s and projetos_acordados is not null', [bid])
        except Exception as e:
            print(e)
            exit(-2)
        
        res = cur.fetchone()[0]
        conn.commit()
        conn.close()
        
        return res
        
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

