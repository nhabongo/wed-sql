from BaseWorker import BaseClass
import sys

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'verificar_claa'
    dbs = 'user=coleta dbname=coleta application_name=ww-tr_aut'
    wakeup_interval = 5
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        
        return "claa=(false)::bool, motivo_falha_coleta='autopsia finalizou antes de liberar a coleta'"
        #return None
        
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

