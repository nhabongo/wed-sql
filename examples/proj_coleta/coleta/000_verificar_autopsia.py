from BaseWorker import BaseClass
import sys,random

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'verificar_autopsia'
    dbs = 'user=coleta dbname=coleta application_name=ww-tr_aut'
    wakeup_interval = 5
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        
        if random.random() < 0.5:
            res = 'false'
        else:
            res = 'true'
        
        return "autopsia_realizada= '%s'" % (res)
        #return None
        
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

