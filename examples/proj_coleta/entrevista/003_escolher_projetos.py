from BaseWorker import BaseClass
import sys

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'escolher_projetos'
    dbs = 'user=entrevista dbname=entrevista application_name=ww-tr_esc'
    wakeup_interval = 50
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        
        return "projetos_elegiveis='[{\"projeto\":\"pjx\", \"material\":\"cerebro\"},{\"projeto\":\"pjk\", \"material\":\"coracao\"},{\"projeto\":\"pjy\", \"material\":\"rim\"},{\"projeto\":\"pjz\", \"material\":\"figado\"}]', termos_a_aplicar='{t_pjx, t_pjk, t_pjy, t_pjz}'"
        #return None
        
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

