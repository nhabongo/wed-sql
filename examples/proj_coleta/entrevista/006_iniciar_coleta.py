from BaseWorker import BaseClass
import sys,psycopg2

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'iniciar_coleta'
    dbs = 'user=entrevista dbname=entrevista application_name=ww-tr_entr'
    wakeup_interval = 50
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        
        #input("Start new instace at WED-flow Coleta ?")
        self.new_coleta_instance(payload['bid'])
        
        return "coleta_iniciada='true'"
        #return None
   
    def new_coleta_instance(self,bid):
        try:
            conn = psycopg2.connect("dbname=coleta user=coleta")
        except Exception as e:
            print(e)
            exit(-1)
            
        cur = conn.cursor()
        try:
            cur.execute('insert into wed_flow (bid) values (%s)', [bid])
        except Exception as e:
            print(e)
            exit(-2)
        
        print(cur.rowcount)
        conn.commit()
        conn.close()
        
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

