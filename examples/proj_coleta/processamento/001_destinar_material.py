from BaseWorker import BaseClass
import sys,psycopg2,json

class MyWorker(BaseClass):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'destinar_material'
    dbs = 'user=processamento dbname=processamento application_name=ww-tr_send'
    wakeup_interval = 50
    
    def __init__(self):
        super().__init__(MyWorker.trname,MyWorker.dbs,MyWorker.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self,payload):
        print (payload)
        
        projetos_acordados = json.loads(self.get_proj_acord(payload['bid']))
        
        for p in projetos_acordados:
            if p['material'] == payload['material']:
                proj = p['projeto']
                break
        
        print("\033[33mMaterial (%s) enviado ao projeto:\033[0m %s" %(payload['material'],proj))
        return "procedimento_destino= 'material enviado ao projeto %s'" %(proj)
        #return None
    
    def get_proj_acord(self, bid):
        
        try:
            conn = psycopg2.connect("dbname=entrevista user=entrevista")
        except Exception as e:
            print(e)
            exit(-1)
            
        cur = conn.cursor()
        try:
            cur.execute('select projetos_acordados from wed_flow where bid = %s and projetos_acordados is not null', [bid])
        except Exception as e:
            print(e)
            exit(-2)
        
        res = cur.fetchone()[0]
        conn.commit()
        conn.close()
        
        return res
            
w = MyWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

