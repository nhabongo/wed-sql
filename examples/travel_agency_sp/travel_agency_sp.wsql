create flow travel_agency_sp;
\cw travel_agency_sp;
begin;

create wed-attribute customer_id;
create wed-attribute customer_status;
create wed-attribute air_ticket_id;
create wed-attribute air_ticket_status;
create wed-attribute hotel_id;
create wed-attribute hotel_status;
create wed-attribute order_id;
create wed-attribute order_status;

create wed-condition c_cancel as (hotel_status='Requested');
create wed-condition c_new_travel_request as (customer_status = 'Not Validated' AND order_status = 'Received');
create wed-condition c_hotel_requested as (hotel_status = 'Requested' AND order_status = 'Validated');
create wed-condition c_air_ticket_requested as (air_ticket_status = 'Requested' AND order_status = 'Validated');
create wed-condition c_request_treated as ((hotel_status = 'Reserved' or hotel_status = 'Canceled') AND air_ticket_status = 'Purchased' AND order_status = 'Validated');

create wed-condition sc_hotel as (hotel_status='Canceled');
create wed-condition sc_air_ticket as (air_ticket_status = 'Canceled');
create wed-condition sc_cancel as ((hotel_status = 'Canceled' OR hotel_status = 'Reserved') AND air_ticket_status = 'Purchased');

create wed-transition t_validate_travel_request stop on timeout '1h';
create wed-transition t_buy_air_ticket stop on timeout '10m' condition sc_air_ticket;
create wed-transition t_reserve_hotel stop on timeout '10m' condition sc_hotel;
create wed-transition t_close_travel_request;
create wed-transition t_cancel_air_hotel stop on timeout '10m' condition sc_cancel;

create wed-trigger wed_trigger1 as (c_new_travel_request, t_validate_travel_request);
create wed-trigger wed_trigger2 as (c_hotel_requested, t_reserve_hotel);
create wed-trigger wed_trigger3 as (c_air_ticket_requested, t_buy_air_ticket);
create wed-trigger wed_trigger4 as (c_request_treated, t_close_travel_request);
create wed-trigger cancel as (c_cancel, t_cancel_air_hotel);

set final wed-state as (customer_status = 'Validated' AND air_ticket_status = 'Purchased' AND (hotel_status = 'Reserved' OR hotel_status = 'Canceled') AND order_status = 'Finalized');

CREATE OR REPLACE FUNCTION t_buy_air_ticket (s wed_flow) RETURNS wed_flow AS
$$
    import time
    
    plpy.info('t_buy_air_ticket: %s' % str(s))
    
    s['air_ticket_status'] = 'Purchased'
    time.sleep(3)
    return s

$$ LANGUAGE plpython3u SET search_path = 'public';

CREATE OR REPLACE FUNCTION t_reserve_hotel (s wed_flow) RETURNS wed_flow AS
$$
    import time
    
    plpy.info('t_reserve_hotel: %s' % str(s))
    
    s['hotel_status'] = 'Reserved'
    time.sleep(5)
    return s

$$ LANGUAGE plpython3u SET search_path = 'public';

commit;

create instance as (customer_status, order_status) values ('Not Validated', 'Received');

terminate instance 1;

reinstate instance 1;
