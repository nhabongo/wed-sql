from WEDWorker import WEDWorkerTemplate
import sys,random

class IniciaPedidoWorker(WEDWorkerTemplate):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'inicia_pedido'
    dbs = 'user=travel_agency dbname=travel_agency application_name=IniciaPedidoWorker'
    wakeup_interval = 5
    
    def __init__(self):
        super().__init__(self.trname, self.dbs, self.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self, wed_state, wid):
        print("Novo pedido (Id = %d)" % wid)
        print ("Reservas solicitadas: %s" % wed_state['res_solicitadas'])
        
        res = wed_state['res_solicitadas'].split(',')
        
        res_voo = res_hotel = res_auto = 'ignorar'
            
        if 'voo' in res:
            res_voo = 'requisitar'
        if 'hotel' in res:
            res_hotel = 'requisitar'
        if 'carro' in res:
            res_auto = 'requisitar'
        
        #return "autopsia_realizada= '%s'" % (res)
        return "res_voo = '%s', res_hotel = '%s', res_auto = '%s', status = 'aberto'" % (res_voo, res_hotel, res_auto)
        
w = IniciaPedidoWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

