from WEDWorker import WEDWorkerTemplate
import sys,random

class ReservaHotelWorker(WEDWorkerTemplate):
    
    trname = 'reserva_hotel'
    dbs = 'user=travel_agency dbname=travel_agency application_name=ReservaHotelWorker'
    wakeup_interval = 5
    
    def __init__(self):
        super().__init__(self.trname, self.dbs, self.wakeup_interval)
    
    def wed_trans(self, wed_state, wid):
        print("Efetuando reserva de quarto de hotel (pedido Id = %d)" % wid)

        return "res_hotel = 'reservado'"
        
w = ReservaHotelWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

