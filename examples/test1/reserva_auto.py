from WEDWorker import WEDWorkerTemplate
import sys,random

class ReservaAutoWorker(WEDWorkerTemplate):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'reserva_auto'
    dbs = 'user=travel_agency dbname=travel_agency application_name=ReservaAutoWorker'
    wakeup_interval = 5
    
    def __init__(self):
        super().__init__(self.trname, self.dbs, self.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self, wed_state, wid):
        print("Reserva de Automovel (pedido Id = %d)" % wid)
        print("Efetuando a reserva do veiculo")
        
        
        #return "autopsia_realizada= '%s'" % (res)
        return "res_auto = 'reservado'"
        
w = ReservaAutoWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

