from WEDWorker import WEDWorkerTemplate
import sys,random

class FinalizaPedidoWorker(WEDWorkerTemplate):
    
    #trname and dbs variables are static in order to conform with the definition of wed_trans()    
    trname = 'finaliza_pedido'
    dbs = 'user=travel_agency dbname=travel_agency application_name=FinalizaPedidoWorker'
    wakeup_interval = 5
    
    def __init__(self):
        super().__init__(self.trname, self.dbs, self.wakeup_interval)
    
    # Compute the WED-transition and return a string as the new WED-state, using the SQL SET clause syntax 
    # Return None to abort transaction
    def wed_trans(self, wed_state, wid):
        print("Finalizando pedido (Id = %d)" % wid)
        print ("Reservas solicitadas: %s" % wed_state['res_solicitadas'])
        print ("Efetuando pagamento ...")
        print ("Emitindo documentos ...")
        print ("Boa viagem !")        
        
        return "status = 'fechado'"
        
w = FinalizaPedidoWorker()

try:
    w.run()
except KeyboardInterrupt:
    print()
    sys.exit(0)

